<!DOCTYPE html>
<html lang="ua">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="HandheldFriendly" content="true">

  <meta name="description" content="This is description">
  <meta name="keywords" content="keywords">
  <title>Активний сусід</title>
  <link rel="shortcut icon" href="img/favicon.jpg" type="image/x-icon">
  <link rel="icon" href="img/favicon.jpg" type="image/x-icon">

  <style>
    #page-preloader {
      position: fixed;
      left: 0;
      top: 0;
      right: 0;
      bottom: 0;
      background: #63b6ec;
      z-index: 100500;
    }
    #page-preloader .spinner {
      width: 52px;
      height: 52px;
      position: absolute;
      left: 50%;
      top: 50%;
      margin: -26px 0 0 -26px;
      background: url('img/favicon.png') no-repeat 50% 50%;
      -webkit-animation-duration: 1s;
      animation-duration: 1s;
      -webkit-animation-fill-mode: both;
      animation-fill-mode: both;
    }
    @-webkit-keyframes preloader-anim {
      0% {
        opacity: 1;
        -webkit-transform: translateY(100px) translateX(-100px);
        transform: translateY(100px) translateX(-100px);
      }

      100% {
        opacity: 1;
        -webkit-transform: translateY(-100px) translateX(100px);
        transform: translateY(-100px) translateX(100px);
      }
    }
    @keyframes preloader-anim {
      0% {
        opacity: 1;
        -webkit-transform: translateY(100px) translateX(-100px);
        -ms-transform: translateY(100px) translateX(-100px);
        transform: translateY(100px) translateX(-100px);
      }

      100% {
        opacity: 1;
        -webkit-transform: translateY(-100px) translateX(100px);
        -ms-transform: translateY(-100px) translateX(100px);
        transform: translateY(-100px) translateX(100px);
      }
    }
    .preloader-anim {
      -webkit-animation-name: preloader-anim;
      animation-name: preloader-anim;
    }
  </style>

  <!-- Styles -->
    <!-- Libs -->
      <link rel="stylesheet" type="text/css" href="css/libs.css">
    <!-- Common -->
      <link rel="stylesheet" type="text/css" href="css/common.css">
    <!-- Custom -->
      <link rel="stylesheet" type="text/css" href="css/problem.css">

  <!--[if lt IE 9]>v
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyC6HjX8Vp_hwJmQisJGsQfaC8jXIokZUxA&extension=.js"></script>
  <script>
    function addYourLocationButton(map, marker) 
    {
        var controlDiv = document.createElement('div');
        controlDiv.className = "my_location";
        
        var firstChild = document.createElement('button');
        firstChild.style.backgroundColor = '#3498DB';
        firstChild.style.border = 'none';
        firstChild.style.outline = 'none';
        firstChild.style.width = '40px';
        firstChild.style.height = '40px';
        firstChild.style.borderRadius = '2px';
        firstChild.style.boxShadow = '0 1px 4px rgba(0,0,0,0.3)';
        firstChild.style.cursor = 'pointer';
        firstChild.style.marginRight = '10px';
        firstChild.style.padding = '0px';
        firstChild.title = 'Your Location';
        controlDiv.appendChild(firstChild);
        
        var secondChild = document.createElement('div');
        secondChild.style.margin = '0';
        secondChild.style.width = '40px';
        secondChild.style.height = '40px';
        secondChild.style.backgroundImage = 'url(img/icons/cursor.png)';
        secondChild.style.backgroundPosition = 'center';
        secondChild.style.backgroundRepeat = 'no-repeat';
        secondChild.id = 'you_location_img';
        firstChild.appendChild(secondChild);
        
        google.maps.event.addListener(map, 'dragend', function() {
            $('#you_location_img').css('background-position', 'center');
        });

        firstChild.addEventListener('click', function() {
            var imgX = '0';
            var animationInterval = setInterval(function(){
                if(imgX == '-18') imgX = '0';
                else imgX = '-18';
                $('#you_location_img').css('background-position', 'center');
            }, 500);
            if(navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position) {
                    var latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                    marker.setPosition(latlng);
                    map.setCenter(latlng);
                    clearInterval(animationInterval);
                    $('#you_location_img').css('background-position', 'center');
                });
            }
            else{
                clearInterval(animationInterval);
                $('#you_location_img').css('background-position', 'center');
            }
        });
        
        controlDiv.index = 1;
        map.controls[google.maps.ControlPosition.RIGHT_CENTER].push(controlDiv);
    }
  </script>

</head>
<body>
  
  <!-- START Preloader -->
    <div id="page-preloader"><span class="spinner preloader-anim"></span></div>
  <!-- Finish Preloader -->

  <?php
    include 'php-components/map.php';
  ?>

  <?php
    include 'php-components/header.php';
  ?>
  
  <main>
    <div class="container">
      <div class="info">
        <div class="flex-with-qr">
          <div>
            <a href="problems.php" class="back">< Повернутися в каталог ініціативи</a>
            <p class="problem-number">
              Ініціативна № 2524256
            </p>
            <p class="problem-name">
              Намалювати мурал
            </p>
            <p class="problem-address"><img src="img/icons/marker-gray.png" alt="icon">Чернігівська обл, Чернігів, Козацька, 9</p>
          </div>
          <img src="img/icons/qr.png" alt="img">
        </div>
        <p class="problem-text">Добрий день адміністрація! Відповіді не отримали. Документів наших немає. Відділ архітектури ...</p>
        <a href="#" class="problem-text-more">Читати повністю</a>
        <div class="problem-buttons">
          <a href="#" class="green-button">ВІДСТЕЖУВАТИ</a>
          <a href="#" class="greenlight-button">ДОЛУЧИТИСЬ</a>
        </div>
        <div class="problem-creator">
          <div class="problem-creator-flex">
            <a href="profile.php" class="user">
              <div class="avatar">
                <img src="img/photos/avatar.jpg" alt="photo">
              </div>
              <div>
                <p class="user-name">Артем Шаріков</p>
                <p class="problem-time">15.06.2018 в 17:72</p>
              </div>
            </a>
            <div class="problem-social">
              <a href="#">
                <img src="img/icons/printer.png" alt="img">
              </a>
              <a href="#">
                <img src="img/icons/twitter-gray.png" alt="img">
              </a>
              <a href="#">
                <img src="img/icons/facebook-gray.png" alt="img">
              </a>
              <a href="#">
                <img src="img/icons/arrows.png" alt="img">
              </a>
            </div>
          </div>
          <div class="counter-list">
            <a href="#" class="counter"><span>1</span> Ініціатив</a>
            <a href="#" class="counter"><span>1</span> Заявок</a>
            <a href="#" class="counter"><span>1</span> Проблем на <br>розгляді</a>
          </div>
          <div class="problems-photos">
            <p data-fancybox-trigger="gallery"><span>Фотогалерея</span><span>7 фото</span></p>
            <a class="cover-img" data-fancybox="gallery" href="img/photos/1.jpg"><img src="img/photos/1.jpg" alt="photo"></a>
            <a data-fancybox="gallery" href="img/photos/2.jpg"><img src="img/photos/2.jpg" alt="photo"></a>
            <a data-fancybox="gallery" href="img/photos/3.jpg"><img src="img/photos/3.jpg" alt="photo"></a>
            <a data-fancybox="gallery" href="img/photos/1.jpg"><img src="img/photos/1.jpg" alt="photo"></a>
            <a data-fancybox="gallery" href="img/photos/2.jpg"><img src="img/photos/2.jpg" alt="photo"></a>
            <a data-fancybox="gallery" href="img/photos/3.jpg"><img src="img/photos/3.jpg" alt="photo"></a>
            <a data-fancybox="gallery" href="img/photos/1.jpg"><img src="img/photos/1.jpg" alt="photo"></a>
          </div>
        </div>
      </div>
      <div class="tabs" id="tabs">
        <ul>
          <li>
            <a href="#decision-course">ХІД ВИРІШЕННЯ</a>
          </li>
          <li>
            <a href="#comments">КОМЕНТАРІ</a>
          </li>
          <li>
            <a href="#signs">ПІДПИСИ ІНІЦІАТИВИ</a>
          </li>
        </ul>

        <div id="decision-course" class="decision-course tab">
          <div class="item">
            <div class="insert">
              <div class="date">03 вер. <br>в 17:14</div>
              <div class="icon">
                <img src="img/icons/moderation.png" alt="icon">
              </div>
              <div class="text">Проблема №1953388 зареєстрована в системі і відправлена на модерацію.</div>
            </div>
          </div>
          <div class="item">
            <div class="insert">
              <div class="date">03 вер. <br>в 17:14</div>
              <div class="icon">
                <img src="img/icons/problem-registration.png" alt="icon">
              </div>
              <div class="text">Повідомлення успішно зареєстровано в системі і прийнято КП АТП 2528.</div>
            </div>
            <div class="progress">
              <p>
                100 з 500 підписів
              </p>
              <div class="progress-bar">
                <div class="bar" style="width: 20%"></div>
              </div>
            </div>
          </div>
          <div class="item">
            <div class="insert">
              <div class="date">03 вер. <br>в 17:14</div>
              <div class="icon">
                <img src="img/photos/avatar.jpg" alt="img">
              </div>
              <div class="text">Дякую чекаю, виконання.</div>
            </div>
          </div>
          <form action="#" class="item">
            <p>Залишити коментар <span>(залишилось 700 символів)</span></p>
            <textarea placeholder="Ваш коментар"></textarea>
            <button type="submit">НАДІСЛАТИ</button>
          </form>
        </div>
        <div id="comments" class="comments tab">
          <div class="item">
            <div class="insert">
              <div class="date">03 вер. <br>в 17:14</div>
              <div class="icon">
                <img src="img/icons/moderation.png" alt="icon">
              </div>
              <div class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deserunt magnam laboriosam harum a dolor, quod eius maiores necessitatibus? Porro, eaque.</div>
            </div>
          </div>
          <div class="item">
            <div class="insert">
              <div class="date">03 вер. <br>в 17:14</div>
              <div class="icon">
                <img src="img/icons/problem-registration.png" alt="icon">
              </div>
              <div class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda, quibusdam!</div>
            </div>
          </div>
          <div class="item">
            <div class="insert">
              <div class="date">03 вер. <br>в 17:14</div>
              <div class="icon">
                <img src="img/photos/avatar.jpg" alt="img">
              </div>
              <div class="text">Дякую чекаю, виконання.</div>
            </div>
          </div>
          <form action="#" class="item">
            <p>Залишити коментар <span>(залишилось 700 символів)</span></p>
            <textarea placeholder="Ваш коментар"></textarea>
            <button type="submit">НАДІСЛАТИ</button>
          </form>
        </div>
        <div id="signs" class="signs tab">
          <div class="signs-list">
            <div class="item sign-item">
              <div class="insert">
                <div class="icon">
                  <img src="img/photos/avatar.jpg" alt="img">
                </div>
                <div class="name">Міщенко Олена Володимирівна</div>
                <div class="sign-date">29 жовтня 2018. 15:56</div>
              </div>
            </div>
            <div class="item sign-item">
              <div class="insert">
                <div class="icon">
                  <img src="img/photos/avatar.jpg" alt="img">
                </div>
                <div class="name">Міщенко Олена Володимирівна</div>
                <div class="sign-date">29 жовтня 2018. 15:56</div>
              </div>
            </div>
            <div class="item sign-item">
              <div class="insert">
                <div class="icon">
                  <img src="img/photos/avatar.jpg" alt="img">
                </div>
                <div class="name">Міщенко Олена Володимирівна</div>
                <div class="sign-date">29 жовтня 2018. 15:56</div>
              </div>
            </div>
            <div class="item sign-item">
              <div class="insert">
                <div class="icon">
                  <img src="img/photos/avatar.jpg" alt="img">
                </div>
                <div class="name">Міщенко Олена Володимирівна</div>
                <div class="sign-date">29 жовтня 2018. 15:56</div>
              </div>
            </div>
            <div class="item sign-item">
              <div class="insert">
                <div class="icon">
                  <img src="img/photos/avatar.jpg" alt="img">
                </div>
                <div class="name">Міщенко Олена Володимирівна</div>
                <div class="sign-date">29 жовтня 2018. 15:56</div>
              </div>
            </div>
            <div class="item sign-item">
              <div class="insert">
                <div class="icon">
                  <img src="img/photos/avatar.jpg" alt="img">
                </div>
                <div class="name">Міщенко Олена Володимирівна</div>
                <div class="sign-date">29 жовтня 2018. 15:56</div>
              </div>
            </div>
            <div class="item sign-item">
              <div class="insert">
                <div class="icon">
                  <img src="img/photos/avatar.jpg" alt="img">
                </div>
                <div class="name">Міщенко Олена Володимирівна</div>
                <div class="sign-date">29 жовтня 2018. 15:56</div>
              </div>
            </div>
            <div class="item sign-item">
              <div class="insert">
                <div class="icon">
                  <img src="img/photos/avatar.jpg" alt="img">
                </div>
                <div class="name">Міщенко Олена Володимирівна</div>
                <div class="sign-date">29 жовтня 2018. 15:56</div>
              </div>
            </div>
            <div class="item sign-item">
              <div class="insert">
                <div class="icon">
                  <img src="img/photos/avatar.jpg" alt="img">
                </div>
                <div class="name">Міщенко Олена Володимирівна</div>
                <div class="sign-date">29 жовтня 2018. 15:56</div>
              </div>
            </div>
            <div class="item sign-item">
              <div class="insert">
                <div class="icon">
                  <img src="img/photos/avatar.jpg" alt="img">
                </div>
                <div class="name">Міщенко Олена Володимирівна</div>
                <div class="sign-date">29 жовтня 2018. 15:56</div>
              </div>
            </div>
            <div class="item sign-item">
              <div class="insert">
                <div class="icon">
                  <img src="img/photos/avatar.jpg" alt="img">
                </div>
                <div class="name">Міщенко Олена Володимирівна</div>
                <div class="sign-date">29 жовтня 2018. 15:56</div>
              </div>
            </div>
            <div class="item sign-item">
              <div class="insert">
                <div class="icon">
                  <img src="img/photos/avatar.jpg" alt="img">
                </div>
                <div class="name">Міщенко Олена Володимирівна</div>
                <div class="sign-date">29 жовтня 2018. 15:56</div>
              </div>
            </div>
          </div>
          <div class="signs-buttons item">
            <div class="insert">
              <a href="#" class="back"><img src="img/icons/arrow.png" alt="arrow"></a>
              <a href="#"><span>Дивитися далі</span> <img src="img/icons/arrow.png" alt="arrow"></a>
            </div>
          </div>
        </div>
      </div>
      <div class="for-map">
        <div id="map-problem"></div>
        <a href="#" class="modal-add-problem-init">ДОДАТИ ПРОБЛЕМУ</a>
      </div>
    </div>
  </main>
    
  <?php
    include 'php-components/footer.php';
  ?>
    
  <!-- Scripts -->
    <!-- Libs -->
      <script defer src="js/libs.min.js"></script>
    <!-- Common -->
      <script defer src="js/common.min.js"></script>
    <!-- Сustom -->
      <script defer src="js/problem.min.js"></script>
</body>
</html>