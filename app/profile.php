<!DOCTYPE html>
<html lang="ua">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="HandheldFriendly" content="true">

  <meta name="description" content="This is description">
  <meta name="keywords" content="keywords">
  <title>Активний сусід</title>
  <link rel="shortcut icon" href="img/favicon.jpg" type="image/x-icon">
  <link rel="icon" href="img/favicon.jpg" type="image/x-icon">

  <style>
    #page-preloader {
      position: fixed;
      left: 0;
      top: 0;
      right: 0;
      bottom: 0;
      background: #fff;
      z-index: 100500;
    }
    #page-preloader .spinner {
      width: 52px;
      height: 52px;
      position: absolute;
      left: 50%;
      top: 50%;
      margin: -26px 0 0 -26px;
      background: url('img/favicon.png') no-repeat 50% 50%;
      -webkit-animation-duration: 1s;
      animation-duration: 1s;
      -webkit-animation-fill-mode: both;
      animation-fill-mode: both;
    }
    @-webkit-keyframes preloader-anim {
      0% {
        opacity: 0;
        -webkit-transform: translateY(100px);
        transform: translateY(100px);
      }

      100% {
        opacity: 1;
        -webkit-transform: translateY(0);
        transform: translateY(0);
      }
    }
    @keyframes preloader-anim {
      0% {
        opacity: 0;
        -webkit-transform: translateY(100px);
        -ms-transform: translateY(100px);
        transform: translateY(100px);
      }

      100% {
        opacity: 1;
        -webkit-transform: translateY(0);
        -ms-transform: translateY(0);
        transform: translateY(0);
      }
    }
    .preloader-anim {
      -webkit-animation-name: preloader-anim;
      animation-name: preloader-anim;
    }
  </style>

  <!-- Styles -->
    <!-- Libs -->
      <link rel="stylesheet" type="text/css" href="css/libs.css">
    <!-- Common -->
      <link rel="stylesheet" type="text/css" href="css/common.css">
    <!-- Custom -->
      <link rel="stylesheet" type="text/css" href="css/profile.css">

  <!--[if lt IE 9]>v
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyC6HjX8Vp_hwJmQisJGsQfaC8jXIokZUxA&extension=.js"></script>
  <script>
    function addYourLocationButton(map, marker) 
    {
        var controlDiv = document.createElement('div');
        controlDiv.className = "my_location";
        
        var firstChild = document.createElement('button');
        firstChild.style.backgroundColor = '#3498DB';
        firstChild.style.border = 'none';
        firstChild.style.outline = 'none';
        firstChild.style.width = '40px';
        firstChild.style.height = '40px';
        firstChild.style.borderRadius = '2px';
        firstChild.style.boxShadow = '0 1px 4px rgba(0,0,0,0.3)';
        firstChild.style.cursor = 'pointer';
        firstChild.style.marginRight = '10px';
        firstChild.style.padding = '0px';
        firstChild.title = 'Your Location';
        controlDiv.appendChild(firstChild);
        
        var secondChild = document.createElement('div');
        secondChild.style.margin = '0';
        secondChild.style.width = '40px';
        secondChild.style.height = '40px';
        secondChild.style.backgroundImage = 'url(img/icons/cursor.png)';
        secondChild.style.backgroundPosition = 'center';
        secondChild.style.backgroundRepeat = 'no-repeat';
        secondChild.id = 'you_location_img';
        firstChild.appendChild(secondChild);
        
        google.maps.event.addListener(map, 'dragend', function() {
            $('#you_location_img').css('background-position', 'center');
        });

        firstChild.addEventListener('click', function() {
            var imgX = '0';
            var animationInterval = setInterval(function(){
                if(imgX == '-18') imgX = '0';
                else imgX = '-18';
                $('#you_location_img').css('background-position', 'center');
            }, 500);
            if(navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position) {
                    var latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                    marker.setPosition(latlng);
                    map.setCenter(latlng);
                    clearInterval(animationInterval);
                    $('#you_location_img').css('background-position', 'center');
                });
            }
            else{
                clearInterval(animationInterval);
                $('#you_location_img').css('background-position', 'center');
            }
        });
        
        controlDiv.index = 1;
        map.controls[google.maps.ControlPosition.RIGHT_CENTER].push(controlDiv);
    }
  </script>

</head>
<body>
  
  <!-- START Preloader -->
    <div id="page-preloader"><span class="spinner preloader-anim"></span></div>
  <!-- Finish Preloader -->

  <?php
    include 'php-components/map.php';
  ?>

  <?php
    include 'php-components/header.php';
  ?>
  
  <main class="main-width-sidebars">
    <div class="container">
      <div class="left-sidebar main-block">
        <div class="user-container">
          <a href="#" class="user-avatar">
            <img src="img/photos/avatar.jpg" alt="img">
          </a>
          <a href="#" class="user-name">Артем Шакіров</a>
          <a href="#" class="city">Чернігів</a>
        </div>
        <a href="#" class="counter"><span>1</span> Ініціатив</a>
        <a href="#" class="counter"><span>3</span> Проблем</a>
        <a href="#" class="counter"><span>5</span> Проблем на <br>розгляді</a>
      </div>
      <div class="middle-main main-block">
        <p class="middle-main-title">Заявки користувача</p>
        <div class="user-problems-list">
          <?php
            include 'php-components/problem-item.php';
          ?>
          <?php
            include 'php-components/problem-item.php';
          ?>
          <?php
            include 'php-components/problem-item.php';
          ?>
          <?php
            include 'php-components/problem-item.php';
          ?>
          <?php
            include 'php-components/problem-item.php';
          ?>
          <?php
            include 'php-components/problem-item.php';
          ?>
        </div>
      </div>
      <div class="right-sidebar main-block">
        <!-- <div class="filter">
          <p>Показувати спочатку</p>
          <label>
            <input type="checkbox"> <span>Останні додавання</span>
          </label>
          <label>
            <input type="checkbox"> <span>Останні оновлення</span>
          </label>
          <label>
            <input type="checkbox"> <span>Найпопулярніші</span>
          </label>
        </div>
        <div class="filter">
          <p>Статус проблеми</p>
          <label>
            <input type="checkbox"> <span>Отримано відповідь</span>
          </label>
          <label>
            <input type="checkbox"> <span>В роботі</span>
          </label>
          <label>
            <input type="checkbox"> <span>Проблема вирішена</span>
          </label>
        </div> -->
        <div class="users">
          <p class="right-sidebar-item-title">Найактивніші <a href="#">Всі</a></p>
          <a href="#" class="user">
            <div class="avatar">
              <img src="img/photos/avatar.jpg" alt="img">
            </div>
            <div class="user-info">
              <p class="name">Артем Шакіров</p>
              <p class="city">Чернігів</p>
            </div>
          </a>
          <a href="#" class="user">
            <div class="avatar">
              <img src="img/photos/avatar.jpg" alt="img">
            </div>
            <div class="user-info">
              <p class="name">Артем Шакіров</p>
              <p class="city">Чернігів</p>
            </div>
          </a>
          <a href="#" class="user">
            <div class="avatar">
              <img src="img/photos/avatar.jpg" alt="img">
            </div>
            <div class="user-info">
              <p class="name">Артем Шакіров</p>
              <p class="city">Чернігів</p>
            </div>
          </a>
          <a href="#" class="user">
            <div class="avatar">
              <img src="img/photos/avatar.jpg" alt="img">
            </div>
            <div class="user-info">
              <p class="name">Артем Шакіров</p>
              <p class="city">Чернігів</p>
            </div>
          </a>
          <a href="#" class="user">
            <div class="avatar">
              <img src="img/photos/avatar.jpg" alt="img">
            </div>
            <div class="user-info">
              <p class="name">Артем Шакіров</p>
              <p class="city">Чернігів</p>
            </div>
          </a>
          <a href="#" class="user">
            <div class="avatar">
              <img src="img/photos/avatar.jpg" alt="img">
            </div>
            <div class="user-info">
              <p class="name">Артем Шакіров</p>
              <p class="city">Чернігів</p>
            </div>
          </a>
        </div>
      </div>
    </div>
  </main>
    
  <?php
    include 'php-components/footer.php';
  ?>

  <!-- Modals -->
    
  
  <!-- Scripts -->
    <!-- Libs -->
      <script defer src="js/libs.min.js"></script>
    <!-- Common -->
      <script defer src="js/common.min.js"></script>
</body>
</html>