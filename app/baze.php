<!DOCTYPE html>
<html lang="ua">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="HandheldFriendly" content="true">

  <meta name="description" content="This is description">
  <meta name="keywords" content="keywords">
  <title>Активний сусід</title>
  <link rel="shortcut icon" href="img/favicon.jpg" type="image/x-icon">
  <link rel="icon" href="img/favicon.jpg" type="image/x-icon">

  <style>
    #page-preloader {
      position: fixed;
      left: 0;
      top: 0;
      right: 0;
      bottom: 0;
      background: #63b6ec;
      z-index: 100500;
    }
    #page-preloader .spinner {
      width: 52px;
      height: 52px;
      position: absolute;
      left: 50%;
      top: 50%;
      margin: -26px 0 0 -26px;
      background: url('img/favicon.png') no-repeat 50% 50%;
      -webkit-animation-duration: 1s;
      animation-duration: 1s;
      -webkit-animation-fill-mode: both;
      animation-fill-mode: both;
    }
    @-webkit-keyframes preloader-anim {
      0% {
        opacity: 1;
        -webkit-transform: translateY(100px) translateX(-100px);
        transform: translateY(100px) translateX(-100px);
      }

      100% {
        opacity: 1;
        -webkit-transform: translateY(-100px) translateX(100px);
        transform: translateY(-100px) translateX(100px);
      }
    }
    @keyframes preloader-anim {
      0% {
        opacity: 1;
        -webkit-transform: translateY(100px) translateX(-100px);
        -ms-transform: translateY(100px) translateX(-100px);
        transform: translateY(100px) translateX(-100px);
      }

      100% {
        opacity: 1;
        -webkit-transform: translateY(-100px) translateX(100px);
        -ms-transform: translateY(-100px) translateX(100px);
        transform: translateY(-100px) translateX(100px);
      }
    }
    .preloader-anim {
      -webkit-animation-name: preloader-anim;
      animation-name: preloader-anim;
    }
  </style>

  <!-- Styles -->
    <!-- Libs -->
      <link rel="stylesheet" type="text/css" href="css/libs.css">
    <!-- Common -->
      <link rel="stylesheet" type="text/css" href="css/common.css">
    <!-- Custom -->
      <link rel="stylesheet" type="text/css" href="css/baze.css">

  <!--[if lt IE 9]>v
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyC6HjX8Vp_hwJmQisJGsQfaC8jXIokZUxA&extension=.js"></script>
  <script>
    function addYourLocationButton(map, marker) 
    {
        var controlDiv = document.createElement('div');
        controlDiv.className = "my_location";
        
        var firstChild = document.createElement('button');
        firstChild.style.backgroundColor = '#3498DB';
        firstChild.style.border = 'none';
        firstChild.style.outline = 'none';
        firstChild.style.width = '40px';
        firstChild.style.height = '40px';
        firstChild.style.borderRadius = '2px';
        firstChild.style.boxShadow = '0 1px 4px rgba(0,0,0,0.3)';
        firstChild.style.cursor = 'pointer';
        firstChild.style.marginRight = '10px';
        firstChild.style.padding = '0px';
        firstChild.title = 'Your Location';
        controlDiv.appendChild(firstChild);
        
        var secondChild = document.createElement('div');
        secondChild.style.margin = '0';
        secondChild.style.width = '40px';
        secondChild.style.height = '40px';
        secondChild.style.backgroundImage = 'url(img/icons/cursor.png)';
        secondChild.style.backgroundPosition = 'center';
        secondChild.style.backgroundRepeat = 'no-repeat';
        secondChild.id = 'you_location_img';
        firstChild.appendChild(secondChild);
        
        google.maps.event.addListener(map, 'dragend', function() {
            $('#you_location_img').css('background-position', 'center');
        });

        firstChild.addEventListener('click', function() {
            var imgX = '0';
            var animationInterval = setInterval(function(){
                if(imgX == '-18') imgX = '0';
                else imgX = '-18';
                $('#you_location_img').css('background-position', 'center');
            }, 500);
            if(navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position) {
                    var latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                    marker.setPosition(latlng);
                    map.setCenter(latlng);
                    clearInterval(animationInterval);
                    $('#you_location_img').css('background-position', 'center');
                });
            }
            else{
                clearInterval(animationInterval);
                $('#you_location_img').css('background-position', 'center');
            }
        });
        
        controlDiv.index = 1;
        map.controls[google.maps.ControlPosition.RIGHT_CENTER].push(controlDiv);
    }
  </script>

</head>
<body>
  
  <!-- START Preloader -->
    <div id="page-preloader"><span class="spinner preloader-anim"></span></div>
  <!-- Finish Preloader -->

  <?php
    include 'php-components/map.php';
  ?>

  <?php
    include 'php-components/header.php';
  ?>
  
  <main class="inner-page">
    <div class="cover">
      <div class="initiatives-container">
        <p class="title">
          База знань
        </p>
        <p class="descr">
          Готові діяти самостійно, але не знаєте з чого почати? <br>У базі знань ви знайдете наочні покрокові інструкції по найбільш <br>поширеним проблемних ситуацій.
        </p>
        <div class="flex">
          <form action="#">
            <input type="text" placeholder="Пошук статті">
            <button type="submit">
              <img src="img/icons/search-white.png" alt="icon">
            </button>
          </form>
        </div>
      </div>
    </div>
    <div class="for-tabs">
      <div class="tabs" id="tabs">
        <ul>
          <li>
            <p>
              Категорії
            </p>
          </li>
          <li>
            <a href="#famous">
              <div class="round-icon">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 69.277 19.23">
                  <g id="Group_3500" data-name="Group 3500" transform="translate(-332 -1111)">
                    <g id="Group_3505" data-name="Group 3505" transform="translate(381.293 1111)">
                      <g id="Group_3504" data-name="Group 3504" transform="translate(0 0)">
                        <path id="Path_2795" data-name="Path 2795" class="cls-1" d="M384.172,7.175a2.264,2.264,0,0,0-1.839-1.55l-4.144-.6-1.854-3.755A2.264,2.264,0,0,0,374.292,0h0a2.264,2.264,0,0,0-2.042,1.27L370.4,5.026l-4.144.6a2.278,2.278,0,0,0-1.261,3.886l3,2.922-.707,4.128a2.275,2.275,0,0,0,3.306,2.4l3.706-1.95L378,18.964a2.278,2.278,0,0,0,3.3-2.4l-.709-4.127,3-2.924A2.264,2.264,0,0,0,384.172,7.175Zm-5.624,3.479a2.277,2.277,0,0,0-.655,2.017l.6,3.494-3.138-1.649a2.277,2.277,0,0,0-2.12,0L370.1,16.167l.6-3.495a2.278,2.278,0,0,0-.656-2.016L367.5,8.183l3.509-.511a2.278,2.278,0,0,0,1.715-1.247l1.568-3.179,1.57,3.179a2.278,2.278,0,0,0,1.716,1.246l3.509.509Z" transform="translate(-364.301 0)"/>
                      </g>
                    </g>
                    <g id="Group_3507" data-name="Group 3507" transform="translate(356.646 1111)">
                      <g id="Group_3506" data-name="Group 3506" transform="translate(0 0)">
                        <path id="Path_2796" data-name="Path 2796" class="cls-1" d="M202.022,7.175a2.264,2.264,0,0,0-1.839-1.55l-4.144-.6-1.854-3.755A2.264,2.264,0,0,0,192.141,0h0A2.264,2.264,0,0,0,190.1,1.27l-1.852,3.756-4.144.6a2.278,2.278,0,0,0-1.261,3.886l3,2.922-.707,4.128a2.275,2.275,0,0,0,3.306,2.4l3.706-1.95,3.707,1.948a2.278,2.278,0,0,0,3.3-2.4l-.709-4.127,3-2.924A2.264,2.264,0,0,0,202.022,7.175ZM196.4,10.654a2.277,2.277,0,0,0-.655,2.017l.6,3.494L193.2,14.516a2.277,2.277,0,0,0-2.12,0l-3.138,1.651.6-3.495a2.278,2.278,0,0,0-.656-2.016L185.35,8.183l3.508-.511a2.278,2.278,0,0,0,1.715-1.247l1.568-3.18,1.57,3.179a2.278,2.278,0,0,0,1.716,1.246l3.509.509Z" transform="translate(-182.15 0)"/>
                      </g>
                    </g>
                    <g id="Group_3509" data-name="Group 3509" transform="translate(332 1111)">
                      <g id="Group_3508" data-name="Group 3508" transform="translate(0 0)">
                        <path id="Path_2797" data-name="Path 2797" class="cls-1" d="M19.871,7.175a2.264,2.264,0,0,0-1.839-1.55l-4.144-.6L12.033,1.269A2.264,2.264,0,0,0,9.991,0h0A2.264,2.264,0,0,0,7.948,1.27L6.1,5.026l-4.144.6A2.278,2.278,0,0,0,.69,9.516l3,2.922-.707,4.128a2.275,2.275,0,0,0,3.306,2.4l3.706-1.95L13.7,18.964a2.278,2.278,0,0,0,3.3-2.4L16.3,12.434l3-2.924A2.264,2.264,0,0,0,19.871,7.175Zm-5.624,3.479a2.277,2.277,0,0,0-.655,2.017l.6,3.494-3.138-1.649a2.277,2.277,0,0,0-2.12,0L5.8,16.167l.6-3.494a2.277,2.277,0,0,0-.656-2.016L3.2,8.183l3.508-.511A2.278,2.278,0,0,0,8.423,6.425l1.568-3.18,1.57,3.179A2.278,2.278,0,0,0,13.276,7.67l3.509.509Z" transform="translate(0 0)"/>
                      </g>
                    </g>
                  </g>
                </svg>
              </div>
              <p>ПОПУЛЯРНІ</p>
            </a>
          </li>
          <li>
            <a href="#yard">
              <div class="round-icon">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 49.97 52.083">
                  <g id="smart-home" transform="translate(-6.026 0)">
                    <path id="Path_2731" data-name="Path 2731" class="cls-1" d="M167.3,131.974a8.858,8.858,0,0,1,5.212-2h3.853a1.256,1.256,0,0,0,1.255-1.254V114.808a1.256,1.256,0,0,0-1.255-1.254h-21.6a1.256,1.256,0,0,0-1.254,1.254v13.907a1.256,1.256,0,0,0,1.254,1.254H158.4a3.768,3.768,0,0,1,3.763,3.764v2.875Zm-2.763-14.388a1.254,1.254,0,1,1,2.509,0V123.5a1.254,1.254,0,1,1-2.509,0Zm.367,9.328a1.255,1.255,0,1,1-.367.887A1.266,1.266,0,0,1,164.907,126.914Z" transform="translate(-121.628 -93.641)"/>
                    <path id="Path_2732" data-name="Path 2732" class="cls-1" d="M33.3,7.724,44.175,17.4H52.2L33.294.368a1.434,1.434,0,0,0-1.918,0L6.5,22.733a1.434,1.434,0,0,0,.959,2.5H11.7L31.365,7.724a1.434,1.434,0,0,1,1.938,0Z" transform="translate(0 0)"/>
                    <path id="Path_2733" data-name="Path 2733" class="cls-1" d="M86.213,89.278a7.4,7.4,0,0,0-3.838,1.358L77.007,95.48a2.812,2.812,0,0,1-1.872.831c-1.038,0-2.085-.8-2.085-2.591V90.532A1.256,1.256,0,0,0,71.8,89.278H68.164A3.768,3.768,0,0,1,64.4,85.514V71.607a3.768,3.768,0,0,1,3.764-3.764h6.7l-7.51-6.678L48.492,77.936v23.153a1.434,1.434,0,0,0,1.434,1.434H84.779a1.434,1.434,0,0,0,1.434-1.434V89.278Z" transform="translate(-35.019 -50.44)"/>
                  </g>
                </svg>
              </div>
              <p>ДВІР</p>
            </a>
          </li>
          <li>
            <a href="#roads">
              <div class="round-icon">
                <svg id="_1" data-name="1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 51.57 47.703">
                  <path id="Path_2672" data-name="Path 2672" class="cls-1" d="M4858,6099h51.57v4h-4.384c.387,1.676.774,3.223,1.16,4.964-2.771,0-5.479,0-8.122-.064-.258,0-.58-.451-.709-.773-.258-1.1-.451-2.256-.773-3.353-.129-.322-.516-.773-.773-.773h-22.5c-.387,1.676-.709,3.288-1.031,4.9h-8.9c.322-1.611.581-3.158.967-4.9H4858Z" transform="translate(-4858 -6082.112)"/>
                  <path id="Path_2673" data-name="Path 2673" class="cls-1" d="M5299.225,6218.689c-.129-2.965-.258-6-.387-9.024-.258-5.222-.516-10.379-.838-15.665h5.8c.387,2.579.837,5.286,1.289,7.929.838,5.415,1.676,10.765,2.514,16.18.064.193,0,.386.064.58Z" transform="translate(-5269.637 -6170.987)"/>
                  <path id="Path_2674" data-name="Path 2674" class="cls-1" d="M5101,6217.754c.709-4.577,1.354-9.154,2.128-13.794.515-3.611,1.1-7.22,1.675-10.958h5.673c-.128,3.287-.322,6.574-.452,9.8-.193,4.963-.386,9.992-.58,14.955Z" transform="translate(-5085.336 -6170.053)"/>
                  <path id="Path_2675" data-name="Path 2675" class="cls-1" d="M5281.481,5837c.773,4.964,1.547,9.863,2.385,14.891H5278.9c-.257-5.028-.579-9.927-.9-14.891Z" transform="translate(-5250.926 -5837)"/>
                  <path id="Path_2676" data-name="Path 2676" class="cls-1" d="M5185.544,5837c-.193,4.9-.451,9.863-.709,14.827H5180c.773-4.964,1.548-9.863,2.321-14.827Z" transform="translate(-5159.243 -5837)"/>
                </svg>
              </div>
              <p>ДОРОГИ</p>
            </a>
          </li>
          <li>
            <a href="#chief">
              <div class="round-icon">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50.31 50.963">
                  <g id="_8" data-name="8" transform="translate(-0.374 0)">
                    <g id="Group_3370" data-name="Group 3370" transform="translate(0.374 0)">
                      <path id="Path_2716" data-name="Path 2716" class="cls-1" d="M29.295,33.613a6.354,6.354,0,0,0,.169-1.2c.353-7.727-3.869-14.055-10.2-18.123-3.765-2.421-7.335.218-8.242,3.584-2.333-1.857-6.374.922-4.556,3.974.066.111.135.223.2.335-1.825-.007-7.639-.025-6.011,0C2.582,22.219,7.2,27.4,7.2,27.4V23.028c1.418,2.169,3.231,4.2,5.93,4.5a4.045,4.045,0,0,0,3.082-.907,7.5,7.5,0,0,1,1.7,4.478,21.17,21.17,0,0,0-2.785.133c-3.225.38-5.216,1.091-5.868,4.464-.98,5.089,1.666,8.807,4.521,12.7,2.323,3.164,7.669.1,5.316-3.111-1.924-2.622-3.86-4.776-3.78-7.822a22.88,22.88,0,0,1,5.66-.026A6.445,6.445,0,0,0,24,37.923a33.909,33.909,0,0,0,8.339,13.792c2.767,2.858,7.116-1.5,4.354-4.351A27.236,27.236,0,0,1,29.295,33.613Z" transform="translate(-0.374 -1.685)"/>
                      <path id="Path_2717" data-name="Path 2717" class="cls-1" d="M6.529,6.948c.008.087.01.175.023.263A5.205,5.205,0,0,0,16.865,5.794a5.13,5.13,0,0,0-.519-1.647c.176-.085.255-.287.2-.707L16.372,2.2A2.541,2.541,0,0,0,13.51.025L7.968.785A2.542,2.542,0,0,0,5.794,3.651L5.93,4.657c-.777.62-1.433,1.4-1.291,2.233C4.763,7.626,5.579,7.443,6.529,6.948Z" transform="translate(-0.909 0)"/>
                      <path id="Path_2718" data-name="Path 2718" class="cls-1" d="M22.894,12.961s12.479,6.69,11,16.551C30.667,37.733,44.777,46.2,52.449,27.24,52.448,27.24,62.218,1.254,22.894,12.961ZM44.021,28.434v1.585H42.334V28.527a4.46,4.46,0,0,1-2.061-.688l-.237-.17.612-1.711.411.274a3.469,3.469,0,0,0,1.879.561c.807,0,1.345-.419,1.345-1.04,0-.419-.167-.875-1.4-1.375-1.3-.51-2.64-1.254-2.64-2.872a2.709,2.709,0,0,1,2.2-2.654V17.311H44.11v1.436a4.116,4.116,0,0,1,1.7.537l.281.166-.638,1.685-.393-.224a3.131,3.131,0,0,0-1.639-.444c-.852,0-1.155.447-1.155.866,0,.454.225.747,1.565,1.3,1.159.472,2.493,1.243,2.493,3.016A2.877,2.877,0,0,1,44.021,28.434Z" transform="translate(-3.215 -1.253)"/>
                    </g>
                  </g>
                </svg>
              </div>
              <p>КОРУПЦІЯ</p>
            </a>
          </li>
          <li>
            <a href="#police">
              <div class="round-icon">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50.523 57.767">
                  <g id="Group_3392" data-name="Group 3392" transform="translate(-927.004 -1648.92)">
                    <g id="_5" data-name="5" transform="translate(927.003 1648.92)">
                      <path id="Path_2685" data-name="Path 2685" class="cls-1" d="M45843.531,9610.4c-2.652,7.572-9.156,13.4-13.484,16.507-4.227-3.106-10.812-8.859-13.469-16.507a71,71,0,0,1-3.707-17.187,30.461,30.461,0,0,0,6.5-.605c3.191-.756,7.117-2.953,10.672-5.45,5.836,3.938,8.789,5,10.676,5.45a31.142,31.142,0,0,0,6.516.605A70.626,70.626,0,0,1,45843.531,9610.4Zm11.734-24.833s-8.406.983-12.645,0c-3.937-.909-12.645-7.571-12.645-7.571-.832.682-8.633,6.737-12.488,7.571-4.168.983-12.57,0-12.57,0a71.128,71.128,0,0,0,4.16,27.03c5.156,14.536,20.969,23.165,20.969,23.165s15.906-8.629,20.977-23.165A70.52,70.52,0,0,0,45855.266,9585.571Z" transform="translate(-45804.828 -9578)"/>
                    </g>
                  </g>
                </svg>
              </div>
              <p>ПОЛІЦІЯ</p>
            </a>
          </li>
          <li>
            <a href="#animals">
              <div class="round-icon">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50.809 44.084">
                  <g id="_6" data-name="6" transform="translate(-0.001)">
                    <path id="Path_2686" data-name="Path 2686" class="cls-1" d="M49588.238,9945.85c-8.867.207-15.1,9.211-15.238,15.586v1.246c.41,5.127,4.574,7.342,9,4.848,1.039-.555,2.078-1.178,3.117-1.8a7.484,7.484,0,0,1,8.8.139,35.025,35.025,0,0,0,3.465,2.146c2.973,1.664,6.023.762,7.477-2.285a9.518,9.518,0,0,0,1.039-3.879c.215-4.432-1.941-8.035-4.777-11.223C49597.734,9946.818,49593.227,9945.709,49588.238,9945.85Z" transform="translate(-49564.547 -9924.754)"/>
                    <path id="Path_2687" data-name="Path 2687" class="cls-1" d="M49605.461,9658.416c1.109-1.869,1.109-3.74,1.875-5.816v-.486a11.3,11.3,0,0,0-1.945-7c-2.84-4.156-7.895-4.156-10.8,0a12.51,12.51,0,0,0,.066,13.578C49597.637,9662.92,49602.762,9662.85,49605.461,9658.416Z" transform="translate(-49582.809 -9641.964)"/>
                    <path id="Path_2688" data-name="Path 2688" class="cls-1" d="M49832.047,9658.063a11.384,11.384,0,0,0,1.945-6.512,12.074,12.074,0,0,0-1.945-6.928c-2.836-4.086-7.687-4.226-10.664-.277a12.542,12.542,0,0,0,0,13.993C49824.359,9662.357,49829.211,9662.219,49832.047,9658.063Z" transform="translate(-49793.734 -9641.471)"/>
                    <path id="Path_2689" data-name="Path 2689" class="cls-1" d="M50026.484,9856.079a4.818,4.818,0,0,0-7.414.069,8.315,8.315,0,0,0-2.07,5.889v.207a13.734,13.734,0,0,0,1.664,5.2c2.008,3.047,5.742,3.116,8.1.346C50029.328,9864.668,50029.188,9858.988,50026.484,9856.079Z" transform="translate(-49977.793 -9839.625)"/>
                    <path id="Path_2690" data-name="Path 2690" class="cls-1" d="M49460.906,9866.976a9.789,9.789,0,0,0,.066-10.6c-2.285-3.326-6.578-3.395-8.66.138a13.68,13.68,0,0,0-1.312,5.4,11.643,11.643,0,0,0,1.59,5.126C49454.672,9870.16,49458.617,9870.022,49460.906,9866.976Z" transform="translate(-49451 -9839.16)"/>
                  </g>
                </svg>
              </div>
              <p>ТВАРИНИ</p>
            </a>
          </li>
        </ul>

        <div id="famous" class="famous tab">
          <?php
            include 'php-components/baze-item.php';
          ?>
          <?php
            include 'php-components/baze-item.php';
          ?>
          <?php
            include 'php-components/baze-item.php';
          ?>
          <?php
            include 'php-components/baze-item.php';
          ?>
          <?php
            include 'php-components/baze-item.php';
          ?>
          <?php
            include 'php-components/baze-item.php';
          ?>
        </div>
        <div id="yard" class="yard tab">
          <?php
            include 'php-components/baze-item.php';
          ?>
          <?php
            include 'php-components/baze-item.php';
          ?>
          <?php
            include 'php-components/baze-item.php';
          ?>
          <?php
            include 'php-components/baze-item.php';
          ?>
        </div>
        <div id="roads" class="roads tab">
          <?php
            include 'php-components/baze-item.php';
          ?>
          <?php
            include 'php-components/baze-item.php';
          ?>
          <?php
            include 'php-components/baze-item.php';
          ?>
          <?php
            include 'php-components/baze-item.php';
          ?>
        </div>
        <div id="chief" class="chief tab">
          <?php
            include 'php-components/baze-item.php';
          ?>
          <?php
            include 'php-components/baze-item.php';
          ?>
          <?php
            include 'php-components/baze-item.php';
          ?>
          <?php
            include 'php-components/baze-item.php';
          ?>
        </div>
        <div id="police" class="police tab">
          <?php
            include 'php-components/baze-item.php';
          ?>
          <?php
            include 'php-components/baze-item.php';
          ?>
          <?php
            include 'php-components/baze-item.php';
          ?>
          <?php
            include 'php-components/baze-item.php';
          ?>
        </div>
        <div id="animals" class="animals tab">
          <?php
            include 'php-components/baze-item.php';
          ?>
          <?php
            include 'php-components/baze-item.php';
          ?>
          <?php
            include 'php-components/baze-item.php';
          ?>
          <?php
            include 'php-components/baze-item.php';
          ?>
        </div>
      </div>
    </div>
  </main>
    
  <?php
    include 'php-components/footer.php';
  ?>

  <!-- Scripts -->
    <!-- Libs -->
      <script defer src="js/libs.min.js"></script>
    <!-- Common -->
      <script defer src="js/common.min.js"></script>
</body>
</html>