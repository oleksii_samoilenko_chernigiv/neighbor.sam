<footer class="footer">
	<div class="social">
		<a href="#" class="social-item fb">
			<img src="img/icons/facebook.png" alt="icon">
			<p>UkraineSmartCity</p>
		</a>
		<a href="#" class="social-item twitter">
			<img src="img/icons/twitter.png" alt="icon">
			<p>@UkraineSmartCity</p>
		</a>
		<a href="#" class="social-item youtube">
			<img src="img/icons/youtube.png" alt="icon">
			<p>Наші відео</p>
		</a>
		<a href="#" class="social-item google">
			<img src="img/icons/google.png" alt="icon">
			<p>Завантажуй додаток Smart Citizen</p>
		</a>
	</div>
	<div class="footer-list">
		<div class="container">
			<div class="usc">
				<img src="img/usc.png" alt="logo">
				<p>Ukraine Smart City - це онлайн-спільнота, в якій сусіди можуть повідомляти владні структури про проблеми які їх турбують та дає можливість сусідам об'єднуватися і вирішувати проблеми своїми силами. Ваша участь має значення!</p>
			</div>
			<div class="lists">
				<ul>
					<li><a href="#">Ukraine Smart City</a></li>
					<li><a href="#">Можливості сервісу</a></li>
					<li><a href="#">Обробка персональних данних</a></li>
					<li><a href="#">Правила сервісу</a></li>
					<li><a href="#">Партнери</a></li>
				</ul>
				<ul>
					<li><a href="#">Будемо на зв'язку</a></li>
					<li><a href="#">Слідкуйте за Facebook</a></li>
					<li><a href="#">Слідкуйте за Twiter</a></li>
					<li><a href="#">Слідкуйте за Youtube</a></li>
					<li><a href="#">Контакти</a></li>
				</ul>
				<ul>
					<li><a href="#">Довідковий центр</a></li>
					<li><a href="#">Як повідомити про проблему?</a></li>
					<li><a href="#">Wiki - знайди відповідь</a></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="copyrights">
		<div class="container">
			<p>Copyright © 2018 Ukraine Smart City всі права захищені. <br><br>Використання матеріалів «Україна Смарт Сіті» дозволено лише при наявності активного посилання на джерело. <br>Всі права на картинки і тексти належать їх авторам.</p>
			<div class="founder">
				<div class="content">
					<p>
						Проект реалізовано<br>
						<span>Неприбутковою Громадською Організацією</span>
					</p>
					<img src="img/founder-logo.png" alt="logo">
				</div>
			</div>
		</div>
	</div>
</footer>