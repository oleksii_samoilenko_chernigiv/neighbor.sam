<div class="problem-item">
  <div class="problem-item-img" style="background-image: url(img/photos/1.jpg);">
    <div class="hat">
      <a href="profile.php" class="user">
        <div class="avatar">
          <img src="img/photos/avatar.jpg" alt="img">
        </div>
        <div class="user-info">
          <p class="name">Артем Шакіров</p>
          <p class="city">м. Чернігів</p>
        </div>
      </a>
      <p class="status"><span></span> в роботі</p>
    </div>
    <img src="img/icons/initiative.png" alt="icon" class="problem-icon">
  </div>
  <div class="problem-item-content">
    <div class="problem-info">
      <p>Ініціативи</p>
      <p class="separator"> | </p>
      <p>15.06.2018 17:72</p>
    </div>
    <a href="initiative.php" class="problem-name">
      Безпритульна собака, біля прокуратури.
    </a>
    <div class="signs-part">
      <div class="progress-bar">
        <div class="bar" style="width: 20%"></div>
      </div>
      <div class="flex">
        <p>
          100 з 500 Підписів
        </p>
        <a href="#">Підписати</a>
      </div>
    </div>
  </div>
  <div class="problem-stat">
    <div class="views"><img src="img/icons/eye.png" alt="img"> 254</div>
    <a href="initiative.php">
      №0462-0001-ІН
    </a>
  </div>
</div>