<div class="map">
	<div id="map"></div>
	<div class="problem-container">
		<a href="#" class="add-problem">Додати проблему</a>
		<?php
			include 'php-components/problem-item.php';
		?>
	</div>
	<div class="map-types">
		<a htef="#" class="type modal-choose-the-type-init">
			<div class="type-icon">
				<img src="img/icons/types/1.png" alt="img">	
			</div>
			<p>Загальні</p>
		</a>
		<a htef="#" class="type modal-choose-the-type-init">
			<div class="type-icon">
				<img src="img/icons/types/2.png" alt="img">	
			</div>
			<p>Транспорт <br>та дороги</p>
		</a>
		<a htef="#" class="type modal-choose-the-type-init">
			<div class="type-icon">
				<img src="img/icons/types/3.png" alt="img">	
			</div>
			<p>Природа <br>та екологія</p>
		</a>
		<a htef="#" class="type modal-choose-the-type-init">
			<div class="type-icon">
				<img src="img/icons/types/4.png" alt="img">	
			</div>
			<p>Благоустрій <br>міста</p>
		</a>
		<a htef="#" class="type modal-choose-the-type-init">
			<div class="type-icon">
				<img src="img/icons/types/5.png" alt="img">	
			</div>
			<p>Житлово- <br>комунальне <br>господарство</p>
		</a>
		<a htef="#" class="type modal-choose-the-type-init">
			<div class="type-icon">
				<img src="img/icons/types/6.png" alt="img">	
			</div>
			<p>Поліція</p>
		</a>
		<a htef="#" class="type modal-choose-the-type-init">
			<div class="type-icon">
				<img src="img/icons/types/7.png" alt="img">	
			</div>
			<p>Тварини</p>
		</a>
		<a htef="#" class="type modal-choose-the-type-init">
			<div class="type-icon">
				<img src="img/icons/types/8.png" alt="img">	
			</div>
			<p>Заклади <br>медицини</p>
		</a>
		<a htef="#" class="type modal-choose-the-type-init">
			<div class="type-icon">
				<img src="img/icons/types/9.png" alt="img">	
			</div>
			<p>Заклади <br>освіти</p>
		</a>
		<a htef="#" class="type modal-choose-the-type-init">
			<div class="type-icon">
				<img src="img/icons/types/10.png" alt="img">
			</div>
			<p>Захист прав <br>споживачів</p>
		</a>
	</div>
	<a href="#" class="map-toggler">
		<img src="img/icons/hand.png" alt="img"> <span class="close">відкрити карту</span><span class="open">згорнути</span>
	</a>
</div>

 <!-- Scripts -->
	<script defer src="js/map.min.js"></script>