<header class="header">
	<div class="container">
		<a href="index.php" class="header-logo">
			<img src="img/logo.png" alt="logo">
		</a>
		<ul>
			<li><a href="index.php">Головна</a></li>
			<li><a href="problems.php">Проблеми</a></li>
			<li><a href="advices.php">Консультацї</a></li>
			<li><a href="treatments.php">Звернення</a></li>
			<li><a href="initiatives.php">Ініціативи</a></li>
			<li><a href="baze.php">База знань</a></li>
			<li><a href="#">Як працює сервіс</a></li>
		</ul>
		<a href="#" class="header-enter modal-login-init">
			Увійти
		</a>
    <!-- Ниже блок, который отображаем вместо копки "Войти" после входа -->
    <!-- <div class="header-account">
      <a href="profile.php">Максим Богомаз <img src="img/icons/account.png" alt="img"></a>
      <a href="#"><img src="img/icons/sign-out.png" alt="img"></a>
    </div> -->
	</div>
</header>

<!-- Modals -->
    <div class="ui modal modal-login">
      <div class="content">
        <p class="modal-title">
          Вхід у систему
        </p>
        <div class="modal-content">
          <form action="#">
            <label class="text-fields">
              <img src="img/icons/user.png" alt="img">
              <input type="text" placeholder="Логін">
            </label>
            <label class="text-fields">
              <img src="img/icons/lock.png" alt="img">
              <input type="password" placeholder="Пароль">
            </label>
            <div class="form-flex">
              <label>
                <input type="checkbox"> Запам'ятати мене
              </label>
              <button type="submit">Увійти</button>
            </div>
            <div class="form-flex">
              <a href="#" class="modal-registration-init">Зареєструватися</a>
              <a href="#" class="modal-password-init">Забули пароль ?</a>
            </div>
          </form>
          <div class="login-through">
            <p>Вхід через соцмережі</p>
            <a href="profile.php">
              <img src="img/icons/fb.png" alt="icon">
            </a>
            <a href="profile.php">
              <img src="img/icons/tw.png" alt="icon">
            </a>
            <a href="profile.php">
              <img src="img/icons/g+.png" alt="icon">
            </a>
          </div>
          <div class="login-through">
            <p>Вхід через</p>
            <a href="#">
              <img src="img/icons/bank1.png" alt="icon">
            </a>
            <a href="#">
              <img src="img/icons/bank2.png" alt="icon">
            </a>
            <a href="#">
              <img src="img/icons/bank3.png" alt="icon">
            </a>
          </div>
          <p class="sign-in-notice">Вхід або Реєстрація через Bank ID <br>дозволяє користувачу розширені <br>можливості сервісу</p>
        </div>
        <div class="actions">
          <a href="#" class="btn btn-outline cancel">
            <img src="img/icons/close.png" alt="icon">
          </a>
        </div>
      </div>
    </div>

    <div class="ui modal modal-registration">
      <div class="content">
        <p class="modal-title">
          Реєстрація
        </p>
        <div class="modal-content">
          <form action="#">
            <div class="registration-flex">
              <div class="flex-item">
                <input type="text" placeholder="Логін">
                <input type="text" placeholder="Прізвище">
                <input type="text" placeholder="Місто">
                <input type="text" placeholder="Будинок" class="small-input">
                <input type="text" placeholder="Квартира" class="small-input">
                <input type="text" placeholder="Стать" class="small-input">
              </div>
              <div class="flex-item">
                <input type="password" placeholder="Пароль">
                <input type="text" placeholder="Ім'я">
                <input type="text" placeholder="Вулиця">
                <input type="tel" placeholder="Номер телефону">
                <input type="email" placeholder="Email">
                <input type="email" placeholder="Повторіть Email">
              </div>
            </div>
            <label>
              <input type="checkbox">
              <p>Я приймаю умови сервісу "Укрейн Смарт Сіті" та даю згоду на обробку моїх <br>персональних данних громадській організації "Активне Суспільство України"</p>
            </label>
            <button type="submit" class="modal-mail-check-init">Реєстрація</button>
          </form>
        </div>
        <div class="actions">
          <a href="#" class="btn btn-outline cancel">
            <img src="img/icons/close.png" alt="icon">
          </a>
        </div>
      </div>
    </div>

    <div class="ui modal modal-mail-check">
      <div class="content">
        <p class="modal-title">
          <img src="img/logo.png" alt="logo">
        </p>
        <div class="modal-content">
          <img src="img/icons/email.png" alt="img">
          <p>
            Будь ласка, перевірте свою електронну пошту
            <br>
            <br>
            <span>На ваш e-mail@domain.com був надісланий <br>електронний лист. Будь ласка, перевірте, <br>чи отримали електронний лист від компанії, <br>і натисніть на посилання</span>
          </p>
          <a href="index.php">
            <img src="img/icons/house.png" alt="img"> Головна
          </a>
        </div>
        <div class="actions">
          <a href="#" class="btn btn-outline cancel modal-mail-check-init">
            <img src="img/icons/close.png" alt="icon">
          </a>
        </div>
      </div>
    </div>

		<div class="ui modal modal-password">
      <div class="content">
        <p class="modal-title">
          Відновлення паролю
        </p>
        <div class="modal-content">
          <p>Введіть вашу електронну адресу</p>
          <form action="#">
            <label>
              <img src="img/icons/mail.png" alt="mail">
              <input type="email" placeholder="e@mail.com">
            </label>
            <button type="submit">Надіслати мій пароль</button>
          </form>
        </div>
        <div class="actions">
          <a href="#" class="btn btn-outline cancel">
          	<img src="img/icons/close.png" alt="icon">
          </a>
        </div>
      </div>
    </div>

    <?php
      include 'php-components/add-problem-modal.php';
    ?>

    <div class="ui modal modal-dropdown modal-choose-the-type">
      <div class="content">
        <p class="modal-title">
          Оберіть тип проблеми
        </p>
        <div class="modal-content">
          <form action="#">
            <div class="cols">
              <label>
                <input type="checkbox" name="city"> <span>Тип проблем</span>
              </label>
              <label>
                <input type="checkbox" name="city"> <span>Тип проблем</span>
              </label>
              <label>
                <input type="checkbox" name="city"> <span>Тип проблем</span>
              </label>
              <label>
                <input type="checkbox" name="city"> <span>Тип проблем</span>
              </label>
              <label>
                <input type="checkbox" name="city"> <span>Тип проблем</span>
              </label>
              <label>
                <input type="checkbox" name="city"> <span>Тип проблем</span>
              </label>
              <label>
                <input type="checkbox" name="city"> <span>Тип проблем</span>
              </label>
              <label>
                <input type="checkbox" name="city"> <span>Тип проблем</span>
              </label>
              <label>
                <input type="checkbox" name="city"> <span>Тип проблем</span>
              </label>
              <label>
                <input type="checkbox" name="city"> <span>Тип проблем</span>
              </label>
              <label>
                <input type="checkbox" name="city"> <span>Тип проблем</span>
              </label>
              <label>
                <input type="checkbox" name="city"> <span>Тип проблем</span>
              </label>
              <label>
                <input type="checkbox" name="city"> <span>Тип проблем</span>
              </label>
              <label>
                <input type="checkbox" name="city"> <span>Тип проблем</span>
              </label>
              <label>
                <input type="checkbox" name="city"> <span>Тип проблем</span>
              </label>
              <label>
                <input type="checkbox" name="city"> <span>Тип проблем</span>
              </label>
              <label>
                <input type="checkbox" name="city"> <span>Тип проблем</span>
              </label>
              <label>
                <input type="checkbox" name="city"> <span>Тип проблем</span>
              </label>
              <label>
                <input type="checkbox" name="city"> <span>Тип проблем</span>
              </label>
              <label>
                <input type="checkbox" name="city"> <span>Тип проблем</span>
              </label>
              <label>
                <input type="checkbox" name="city"> <span>Тип проблем</span>
              </label>
              <label>
                <input type="checkbox" name="city"> <span>Тип проблем</span>
              </label>
              <label>
                <input type="checkbox" name="city"> <span>Тип проблем</span>
              </label>
              <label>
                <input type="checkbox" name="city"> <span>Тип проблем</span>
              </label>
              <label>
                <input type="checkbox" name="city"> <span>Тип проблем</span>
              </label>
              <label>
                <input type="checkbox" name="city"> <span>Тип проблем</span>
              </label>
              <label>
                <input type="checkbox" name="city"> <span>Тип проблем</span>
              </label>
              <label>
                <input type="checkbox" name="city"> <span>Тип проблем</span>
              </label>
              <label>
                <input type="checkbox" name="city"> <span>Тип проблем</span>
              </label>
              <label>
                <input type="checkbox" name="city"> <span>Тип проблем</span>
              </label>
              <label>
                <input type="checkbox" name="city"> <span>Тип проблем</span>
              </label>
              <label>
                <input type="checkbox" name="city"> <span>Тип проблем</span>
              </label>
              <label>
                <input type="checkbox" name="city"> <span>Тип проблем</span>
              </label>
              <label>
                <input type="checkbox" name="city"> <span>Тип проблем</span>
              </label>
              <label>
                <input type="checkbox" name="city"> <span>Тип проблем</span>
              </label>
              <label>
                <input type="checkbox" name="city"> <span>Тип проблем</span>
              </label>
              <label>
                <input type="checkbox" name="city"> <span>Тип проблем</span>
              </label>
              <label>
                <input type="checkbox" name="city"> <span>Тип проблем</span>
              </label>
              <label>
                <input type="checkbox" name="city"> <span>Тип проблем</span>
              </label>
              <label>
                <input type="checkbox" name="city"> <span>Тип проблем</span>
              </label>
            </div>
          </form>
        </div>
        <div class="actions">
          <a href="#" class="btn btn-outline cancel">
            <img src="img/icons/close-gray.png" alt="icon">
          </a>
          <a href="#" class="clear">Скасувати</a>
        </div>
      </div>
    </div>