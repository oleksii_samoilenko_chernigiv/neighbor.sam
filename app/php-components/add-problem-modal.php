<div class="ui modal modal-add-problem">
  <div class="content">
    <p class="modal-title">
      Заповніть форму
    </p>
    <div class="modal-content">
      <form action="#">
        <div class="cols">
          <div class="col col-left">
            <input type="text" placeholder="Короткий заголовок">
            <input type="text" placeholder="Оберіть категорію">
            <div class="file-upload">
              <div class="insert">
                <img src="img/icons/file_upload.png" alt="icon">
                <p>
                  Перетягніть файл <br><span>або <a href="#">оберіть на комп'ютері</a></span>
                </p>
              </div>
            </div>
            <input type="text" placeholder="Посилання на відео на Youtube">
          </div>
          <div class="col col-right">
            <div class="col-right-inputs">
              <input type="text" placeholder="Введіть адресу">
              <textarea placeholder="Опишіть проблему"></textarea>
            </div>
            <div id="map_add_problem"></div>
          </div>
        </div>
        <div class="bottom-flex">
          <p>
            <img src="img/icons/attention.png" alt="icon">Пам'ятайте фото або відео та чіткий короткий опис проблеми допоможе швидко відреагувати.
          </p>
          <button type="submit">
            <img src="img/icons/megafon.png" alt="icon"> Надіслати
          </button>
        </div>
      </form>
    </div>
    <div class="actions">
      <a href="#" class="btn btn-outline cancel">
        <img src="img/icons/close.png" alt="icon">
      </a>
    </div>
  </div>
</div>

<script>
  google.maps.event.addDomListener(window, 'load', init);
    var map, markersArray = [];
    function init() {
        var mapOptions = {
            center: new google.maps.LatLng(51.493865, 31.294811),
            zoom: 14,
            gestureHandling: 'auto',
            fullscreenControl: false,
            zoomControl: true,
            disableDoubleClickZoom: true,
            mapTypeControl: false,
            scaleControl: false,
            scrollwheel: true,
            streetViewControl: false,
            draggable : true,
            clickableIcons: true,
            zoomControlOptions: {
                position: google.maps.ControlPosition.RIGHT_CENTER
            },
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            styles: []
        };

            var mapElement = document.getElementById('map_add_problem');

            var map = new google.maps.Map(mapElement, mapOptions);
            
            var locations = [
                    
                ];
                for (i = 0; i < locations.length; i++) {
                    marker = new google.maps.Marker({
                        icon: locations[i].marker,
                        position: new google.maps.LatLng(locations[i].lat, locations[i].lng),
                        map: map,
                        title: locations[i].title,
                        address: locations[i].address,
                        desc: locations[i].desc,
                        tel: locations[i].tel,
                        int_tel: locations[i].int_tel,
                        vicinity: locations[i].vicinity,
                        open: locations[i].open,
                        open_hours: locations[i].open_hours,
                        photo: locations[i].photo,
                        time: locations[i].time,
                        email: locations[i].email,
                        web: locations[i].web,
                        iw: locations[i].iw
                    });
                    markersArray.push(marker);
                }

            var imageMyPos = 'img/icons/my-location.svg';
            var myMarker = new google.maps.Marker({
                map: map,
                animation: google.maps.Animation.DROP,
                position: {lat: 511.494992, lng: 311.301912},
                icon: imageMyPos
            });
            addYourLocationButton(map, myMarker);
        }
</script>