<div class="problem-item">
  <div class="problem-item-img" style="background-image: url(img/photos/1.jpg);">
    <div class="hat">
      <a href="profile.php" class="user">
        <div class="avatar">
          <img src="img/photos/avatar.jpg" alt="img">
        </div>
        <div class="user-info">
          <p class="name">Артем Шакіров</p>
          <p class="city">м. Чернігів</p>
        </div>
      </a>
      <p class="status"><span></span> в роботі</p>
    </div>
    <img src="img/icons/marker-blue.png" alt="icon" class="problem-icon">
  </div>
  <div class="problem-item-content">
    <div class="problem-info">
      <p>Безпритульні тварини</p>
      <p class="separator"> | </p>
      <p>15.06.2018 17:72</p>
    </div>
    <a href="problem.php" class="problem-name">
      Безпритульна собака, біля прокуратури.
    </a>
    <p class="problem-address">
      Чернігівська область, Чернігів. вулиця Пирогова, 22а
    </p>
  </div>
  <div class="problem-stat">
    <div class="views"><img src="img/icons/eye.png" alt="img"> 254</div>
    <a href="problem.php">
      №0462-0001-че
    </a>
  </div>
</div>