<div class="baze-item item">
	<a href="profile.php" class="user">
		<div class="avatar">
			<img src="img/photos/avatar-2.jpg" alt="photo">
		</div>
		<div>
			<p class="user-name">Анастасія Кузьміна</p>
		</div>
	</a>
	<div class="content">
		<div class="text">
			<p class="title">
				Кожен рецепт на отримання безкоштовних ліків потрібно брати у терапевта. Весь цей процес займає дуже багато часу. Чи не можна якось спростити цю процедуру?
			</p>
			<p class="description">
				Загальний порядок надання громадянам соціальних послуг в частині забезпечення необхідними лікарськими препаратами затверджений Міністерством охороною здоров'я
			</p>
		</div>
	</div>
	<div class="statistic">
		<div class="reaction">
			Корисність відповіді:
			<a href="#" class="thumb-up">
				<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24.123 23.285">
					 <path id="Path_2798" data-name="Path 2798" class="cls-1" d="M22.928,41.941a3.313,3.313,0,0,0-2.145-.8H15.576v-3.76A3.94,3.94,0,0,0,14.2,33.989a4.3,4.3,0,0,0-3.513-.426.6.6,0,0,0-.416.565v4.206A4.155,4.155,0,0,1,8.328,41.8a7.691,7.691,0,0,1-2.056,1.03l-.144.035a1.222,1.222,0,0,0-1.055-.6H1.224A1.226,1.226,0,0,0,0,43.481V54.8A1.226,1.226,0,0,0,1.224,56.02H5.083a1.229,1.229,0,0,0,.941-.441,3.366,3.366,0,0,0,2.447,1.07H18.766c2.274,0,3.726-1.189,3.983-3.27l1.333-8.269A3.488,3.488,0,0,0,22.928,41.941ZM5.113,54.8a.034.034,0,0,1-.035.035H1.224a.034.034,0,0,1-.035-.035V43.481a.034.034,0,0,1,.035-.035H5.083a.034.034,0,0,1,.035.035V54.8ZM22.9,44.923l-1.328,8.283a.054.054,0,0,1,0,.025c-.183,1.481-1.125,2.234-2.8,2.234H8.467a2.179,2.179,0,0,1-2.135-1.828.791.791,0,0,0-.025-.1V44.041l.258-.059c.01,0,.015,0,.025,0A8.6,8.6,0,0,0,9,42.788a5.36,5.36,0,0,0,2.472-4.454V34.6a2.762,2.762,0,0,1,2.036.347,2.861,2.861,0,0,1,.882,2.428v4.35a.6.6,0,0,0,.595.595h5.8a2.116,2.116,0,0,1,1.372.52A2.266,2.266,0,0,1,22.9,44.923Z" transform="translate(0 -33.365)"/>
				</svg>
				125
			</a>
			<a href="#" class="thumb-down">
				<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24.123 23.285">
					 <path id="Path_2798" data-name="Path 2798" class="cls-1" d="M22.928,41.941a3.313,3.313,0,0,0-2.145-.8H15.576v-3.76A3.94,3.94,0,0,0,14.2,33.989a4.3,4.3,0,0,0-3.513-.426.6.6,0,0,0-.416.565v4.206A4.155,4.155,0,0,1,8.328,41.8a7.691,7.691,0,0,1-2.056,1.03l-.144.035a1.222,1.222,0,0,0-1.055-.6H1.224A1.226,1.226,0,0,0,0,43.481V54.8A1.226,1.226,0,0,0,1.224,56.02H5.083a1.229,1.229,0,0,0,.941-.441,3.366,3.366,0,0,0,2.447,1.07H18.766c2.274,0,3.726-1.189,3.983-3.27l1.333-8.269A3.488,3.488,0,0,0,22.928,41.941ZM5.113,54.8a.034.034,0,0,1-.035.035H1.224a.034.034,0,0,1-.035-.035V43.481a.034.034,0,0,1,.035-.035H5.083a.034.034,0,0,1,.035.035V54.8ZM22.9,44.923l-1.328,8.283a.054.054,0,0,1,0,.025c-.183,1.481-1.125,2.234-2.8,2.234H8.467a2.179,2.179,0,0,1-2.135-1.828.791.791,0,0,0-.025-.1V44.041l.258-.059c.01,0,.015,0,.025,0A8.6,8.6,0,0,0,9,42.788a5.36,5.36,0,0,0,2.472-4.454V34.6a2.762,2.762,0,0,1,2.036.347,2.861,2.861,0,0,1,.882,2.428v4.35a.6.6,0,0,0,.595.595h5.8a2.116,2.116,0,0,1,1.372.52A2.266,2.266,0,0,1,22.9,44.923Z" transform="translate(24.123 56.65) rotate(180)"/>
				</svg>
				25
			</a>
		</div>
		<p class="date">
			<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18.322 18.322">
			  <g id="Group_3836" data-name="Group 3836" transform="translate(-1462.068 -1382.172)">
			    <g id="Group_3464" data-name="Group 3464" transform="translate(1462.069 1382.172)">
			      <g id="Group_3463" data-name="Group 3463">
			        <path id="Path_2767" data-name="Path 2767" class="cls-1" d="M16.175,1.431h-.859V0H13.885V1.431H4.437V0H3.006V1.431H2.147A2.15,2.15,0,0,0,0,3.579v12.6a2.15,2.15,0,0,0,2.147,2.147H16.175a2.15,2.15,0,0,0,2.147-2.147V3.579A2.15,2.15,0,0,0,16.175,1.431Zm.716,14.744a.717.717,0,0,1-.716.716H2.147a.717.717,0,0,1-.716-.716V6.728H16.891Zm0-10.879H1.431V3.579a.717.717,0,0,1,.716-.716h.859V4.294H4.437V2.863h9.447V4.294h1.431V2.863h.859a.717.717,0,0,1,.716.716Z"/>
			      </g>
			    </g>
			    <g id="Group_3466" data-name="Group 3466" transform="translate(1464.789 1390.403)">
			      <g id="Group_3465" data-name="Group 3465">
			        <rect id="Rectangle_1717" data-name="Rectangle 1717" class="cls-1" width="1.431" height="1.431"/>
			      </g>
			    </g>
			    <g id="Group_3468" data-name="Group 3468" transform="translate(1467.651 1390.403)">
			      <g id="Group_3467" data-name="Group 3467">
			        <rect id="Rectangle_1718" data-name="Rectangle 1718" class="cls-1" width="1.431" height="1.431"/>
			      </g>
			    </g>
			    <g id="Group_3470" data-name="Group 3470" transform="translate(1470.514 1390.403)">
			      <g id="Group_3469" data-name="Group 3469">
			        <rect id="Rectangle_1719" data-name="Rectangle 1719" class="cls-1" width="1.431" height="1.431"/>
			      </g>
			    </g>
			    <g id="Group_3472" data-name="Group 3472" transform="translate(1473.377 1390.403)">
			      <g id="Group_3471" data-name="Group 3471">
			        <rect id="Rectangle_1720" data-name="Rectangle 1720" class="cls-1" width="1.431" height="1.431"/>
			      </g>
			    </g>
			    <g id="Group_3474" data-name="Group 3474" transform="translate(1476.24 1390.403)">
			      <g id="Group_3473" data-name="Group 3473">
			        <rect id="Rectangle_1721" data-name="Rectangle 1721" class="cls-1" width="1.431" height="1.431"/>
			      </g>
			    </g>
			    <g id="Group_3476" data-name="Group 3476" transform="translate(1464.789 1393.266)">
			      <g id="Group_3475" data-name="Group 3475">
			        <rect id="Rectangle_1722" data-name="Rectangle 1722" class="cls-1" width="1.431" height="1.431"/>
			      </g>
			    </g>
			    <g id="Group_3478" data-name="Group 3478" transform="translate(1467.651 1393.266)">
			      <g id="Group_3477" data-name="Group 3477">
			        <rect id="Rectangle_1723" data-name="Rectangle 1723" class="cls-1" width="1.431" height="1.431"/>
			      </g>
			    </g>
			    <g id="Group_3480" data-name="Group 3480" transform="translate(1470.514 1393.266)">
			      <g id="Group_3479" data-name="Group 3479">
			        <rect id="Rectangle_1724" data-name="Rectangle 1724" class="cls-1" width="1.431" height="1.431"/>
			      </g>
			    </g>
			    <g id="Group_3482" data-name="Group 3482" transform="translate(1473.377 1393.266)">
			      <g id="Group_3481" data-name="Group 3481">
			        <rect id="Rectangle_1725" data-name="Rectangle 1725" class="cls-1" width="1.431" height="1.431"/>
			      </g>
			    </g>
			    <g id="Group_3484" data-name="Group 3484" transform="translate(1464.789 1396.128)">
			      <g id="Group_3483" data-name="Group 3483">
			        <rect id="Rectangle_1726" data-name="Rectangle 1726" class="cls-1" width="1.431" height="1.431"/>
			      </g>
			    </g>
			    <g id="Group_3486" data-name="Group 3486" transform="translate(1467.651 1396.128)">
			      <g id="Group_3485" data-name="Group 3485">
			        <rect id="Rectangle_1727" data-name="Rectangle 1727" class="cls-1" width="1.431" height="1.431"/>
			      </g>
			    </g>
			    <g id="Group_3488" data-name="Group 3488" transform="translate(1470.514 1396.128)">
			      <g id="Group_3487" data-name="Group 3487">
			        <rect id="Rectangle_1728" data-name="Rectangle 1728" class="cls-1" width="1.431" height="1.431"/>
			      </g>
			    </g>
			    <g id="Group_3490" data-name="Group 3490" transform="translate(1473.377 1396.128)">
			      <g id="Group_3489" data-name="Group 3489">
			        <rect id="Rectangle_1729" data-name="Rectangle 1729" class="cls-1" width="1.431" height="1.431"/>
			      </g>
			    </g>
			    <g id="Group_3492" data-name="Group 3492" transform="translate(1476.24 1393.266)">
			      <g id="Group_3491" data-name="Group 3491">
			        <rect id="Rectangle_1730" data-name="Rectangle 1730" class="cls-1" width="1.431" height="1.431"/>
			      </g>
			    </g>
			  </g>
			</svg>
			7.04.2016
		</p>
	</div>
</div>