<!DOCTYPE html>
<html lang="ua">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="HandheldFriendly" content="true">

  <meta name="description" content="This is description">
  <meta name="keywords" content="keywords">
  <title>Активний сусід</title>
  <link rel="shortcut icon" href="img/favicon.jpg" type="image/x-icon">
  <link rel="icon" href="img/favicon.jpg" type="image/x-icon">

  <style>
    #page-preloader {
      position: fixed;
      left: 0;
      top: 0;
      right: 0;
      bottom: 0;
      background: #63b6ec;
      z-index: 100500;
    }
    #page-preloader .spinner {
      width: 52px;
      height: 52px;
      position: absolute;
      left: 50%;
      top: 50%;
      margin: -26px 0 0 -26px;
      background: url('img/favicon.png') no-repeat 50% 50%;
      -webkit-animation-duration: 1s;
      animation-duration: 1s;
      -webkit-animation-fill-mode: both;
      animation-fill-mode: both;
    }
    @-webkit-keyframes preloader-anim {
      0% {
        opacity: 1;
        -webkit-transform: translateY(100px) translateX(-100px);
        transform: translateY(100px) translateX(-100px);
      }

      100% {
        opacity: 1;
        -webkit-transform: translateY(-100px) translateX(100px);
        transform: translateY(-100px) translateX(100px);
      }
    }
    @keyframes preloader-anim {
      0% {
        opacity: 1;
        -webkit-transform: translateY(100px) translateX(-100px);
        -ms-transform: translateY(100px) translateX(-100px);
        transform: translateY(100px) translateX(-100px);
      }

      100% {
        opacity: 1;
        -webkit-transform: translateY(-100px) translateX(100px);
        -ms-transform: translateY(-100px) translateX(100px);
        transform: translateY(-100px) translateX(100px);
      }
    }
    .preloader-anim {
      -webkit-animation-name: preloader-anim;
      animation-name: preloader-anim;
    }
  </style>

  <!-- Styles -->
    <!-- Libs -->
      <link rel="stylesheet" type="text/css" href="css/libs.css">
    <!-- Common -->
      <link rel="stylesheet" type="text/css" href="css/common.css">
    <!-- Custom -->
      <link rel="stylesheet" type="text/css" href="css/advices.css">

  <!--[if lt IE 9]>v
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyC6HjX8Vp_hwJmQisJGsQfaC8jXIokZUxA&extension=.js"></script>
  <script>
    function addYourLocationButton(map, marker) 
    {
        var controlDiv = document.createElement('div');
        controlDiv.className = "my_location";
        
        var firstChild = document.createElement('button');
        firstChild.style.backgroundColor = '#3498DB';
        firstChild.style.border = 'none';
        firstChild.style.outline = 'none';
        firstChild.style.width = '40px';
        firstChild.style.height = '40px';
        firstChild.style.borderRadius = '2px';
        firstChild.style.boxShadow = '0 1px 4px rgba(0,0,0,0.3)';
        firstChild.style.cursor = 'pointer';
        firstChild.style.marginRight = '10px';
        firstChild.style.padding = '0px';
        firstChild.title = 'Your Location';
        controlDiv.appendChild(firstChild);
        
        var secondChild = document.createElement('div');
        secondChild.style.margin = '0';
        secondChild.style.width = '40px';
        secondChild.style.height = '40px';
        secondChild.style.backgroundImage = 'url(img/icons/cursor.png)';
        secondChild.style.backgroundPosition = 'center';
        secondChild.style.backgroundRepeat = 'no-repeat';
        secondChild.id = 'you_location_img';
        firstChild.appendChild(secondChild);
        
        google.maps.event.addListener(map, 'dragend', function() {
            $('#you_location_img').css('background-position', 'center');
        });

        firstChild.addEventListener('click', function() {
            var imgX = '0';
            var animationInterval = setInterval(function(){
                if(imgX == '-18') imgX = '0';
                else imgX = '-18';
                $('#you_location_img').css('background-position', 'center');
            }, 500);
            if(navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position) {
                    var latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                    marker.setPosition(latlng);
                    map.setCenter(latlng);
                    clearInterval(animationInterval);
                    $('#you_location_img').css('background-position', 'center');
                });
            }
            else{
                clearInterval(animationInterval);
                $('#you_location_img').css('background-position', 'center');
            }
        });
        
        controlDiv.index = 1;
        map.controls[google.maps.ControlPosition.RIGHT_CENTER].push(controlDiv);
    }
  </script>

</head>
<body>
  
  <!-- START Preloader -->
    <div id="page-preloader"><span class="spinner preloader-anim"></span></div>
  <!-- Finish Preloader -->

  <?php
    include 'php-components/map.php';
  ?>

  <?php
    include 'php-components/header.php';
  ?>
  
  <main class="inner-page">
    <div class="cover">
      <div class="initiatives-container">
        <p class="title">
          Консультації
        </p>
        <p class="descr">
          Беручи участь в консультаціях, ви допомагаєте керівництву <br>міста почути думку кожного жителя і проводити зміни, які день за днем <br>роблять життя в місті краще.
        </p>
        <div class="flex">
          <div class="number-item">
            <p class="item-title">
              30
            </p>
            <p>
              консультацій <br>на порталі
            </p>
          </div>
          <div class="number-item">
            <p class="item-title">
              1203
            </p>
            <p>
              користувачів взяли <br>участь в консультаціях
            </p>
          </div>
        </div>
      </div>
    </div>
    <div class="undercover">
      <div class="initiatives-container">
        <div class="advices-survey">
          <div class="survey-title">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52.719 52.719">
              <g id="checklist" transform="translate(0.5 0.5)">
                <g id="Group_3496" data-name="Group 3496">
                  <g id="Group_3495" data-name="Group 3495">
                    <path id="Path_2775" data-name="Path 2775" class="cls-1" d="M49.133,0H2.586A2.586,2.586,0,0,0,0,2.586V49.133a2.586,2.586,0,0,0,2.586,2.586H49.133a2.586,2.586,0,0,0,2.586-2.586V2.586A2.586,2.586,0,0,0,49.133,0Zm.862,49.133a.862.862,0,0,1-.862.862H2.586a.862.862,0,0,1-.862-.862V8.62H50V49.133Zm0-42.237H1.724V2.586a.862.862,0,0,1,.862-.862H49.133a.862.862,0,0,1,.862.862Z"/>
                    <path id="Path_2776" data-name="Path 2776" class="cls-1" d="M44.391,34.133h-.862a.862.862,0,0,0,0,1.724h.862a.862.862,0,0,0,0-1.724Z" transform="translate(-38.357 -30.685)"/>
                    <path id="Path_2777" data-name="Path 2777" class="cls-1" d="M87.058,34.133H86.2a.862.862,0,0,0,0,1.724h.862a.862.862,0,1,0,0-1.724Z" transform="translate(-76.714 -30.685)"/>
                    <path id="Path_2778" data-name="Path 2778" class="cls-1" d="M129.725,34.133h-.862a.862.862,0,0,0,0,1.724h.862a.862.862,0,0,0,0-1.724Z" transform="translate(-115.071 -30.685)"/>
                    <path id="Path_2779" data-name="Path 2779" class="cls-1" d="M445.458,34.133H444.6a.862.862,0,0,0,0,1.724h.862a.862.862,0,0,0,0-1.724Z" transform="translate(-398.911 -30.685)"/>
                    <path id="Path_2780" data-name="Path 2780" class="cls-1" d="M45.253,121.277H55.6a2.586,2.586,0,0,0,2.586-2.586v-5.172a2.586,2.586,0,0,0-2.586-2.586H45.253a2.586,2.586,0,0,0-2.586,2.586v5.172A2.586,2.586,0,0,0,45.253,121.277Zm3.448-8.62h6.9a.862.862,0,0,1,.862.862v5.172a.862.862,0,0,1-.862.862H48.7Zm-4.31.862a.862.862,0,0,1,.862-.862h1.724v6.9H45.253a.862.862,0,0,1-.862-.862v-5.172Z" transform="translate(-38.357 -99.727)"/>
                    <path id="Path_2781" data-name="Path 2781" class="cls-1" d="M122.915,153.6h-2.586a.862.862,0,0,0,0,1.724h2.586a.862.862,0,0,0,0-1.724Z" transform="translate(-107.399 -138.084)"/>
                    <path id="Path_2782" data-name="Path 2782" class="cls-1" d="M45.253,249.277H55.6a2.586,2.586,0,0,0,2.586-2.586v-5.172a2.586,2.586,0,0,0-2.586-2.586H45.253a2.586,2.586,0,0,0-2.586,2.586v5.172A2.586,2.586,0,0,0,45.253,249.277Zm3.448-8.62h6.9a.862.862,0,0,1,.862.862v5.172a.862.862,0,0,1-.862.862H48.7Zm-4.31.862a.862.862,0,0,1,.862-.862h1.724v6.9H45.253a.862.862,0,0,1-.862-.862v-5.172Z" transform="translate(-38.357 -214.797)"/>
                    <path id="Path_2783" data-name="Path 2783" class="cls-1" d="M122.915,281.6h-2.586a.862.862,0,0,0,0,1.724h2.586a.862.862,0,1,0,0-1.724Z" transform="translate(-107.399 -253.155)"/>
                    <path id="Path_2784" data-name="Path 2784" class="cls-1" d="M45.253,377.277H55.6a2.586,2.586,0,0,0,2.586-2.586v-5.172a2.586,2.586,0,0,0-2.586-2.586H45.253a2.586,2.586,0,0,0-2.586,2.586v5.172A2.586,2.586,0,0,0,45.253,377.277Zm3.448-8.62h6.9a.862.862,0,0,1,.862.862v5.172a.862.862,0,0,1-.862.862H48.7Zm-4.31.862a.862.862,0,0,1,.862-.862h1.724v6.9H45.253a.862.862,0,0,1-.862-.862v-5.172Z" transform="translate(-38.357 -329.868)"/>
                    <path id="Path_2785" data-name="Path 2785" class="cls-1" d="M122.915,409.6h-2.586a.862.862,0,0,0,0,1.724h2.586a.862.862,0,0,0,0-1.724Z" transform="translate(-107.399 -368.225)"/>
                    <path id="Path_2786" data-name="Path 2786" class="cls-1" d="M222.729,155.325h11.206a.862.862,0,0,0,0-1.724H222.729a.862.862,0,1,0,0,1.724Z" transform="translate(-199.455 -138.085)"/>
                    <path id="Path_2787" data-name="Path 2787" class="cls-1" d="M222.729,283.325h11.206a.862.862,0,0,0,0-1.724H222.729a.862.862,0,1,0,0,1.724Z" transform="translate(-199.455 -253.156)"/>
                    <path id="Path_2788" data-name="Path 2788" class="cls-1" d="M222.729,411.325h11.206a.862.862,0,0,0,0-1.724H222.729a.862.862,0,1,0,0,1.724Z" transform="translate(-199.455 -368.226)"/>
                    <path id="Path_2789" data-name="Path 2789" class="cls-1" d="M377.537,133.875a.862.862,0,0,0,1.219,0l4.31-4.31a.862.862,0,0,0-1.219-1.219l-3.7,3.7-1.115-1.115a.862.862,0,0,0-1.219,1.219Z" transform="translate(-337.634 -115.164)"/>
                    <path id="Path_2790" data-name="Path 2790" class="cls-1" d="M377.537,261.875a.862.862,0,0,0,1.219,0l4.31-4.31a.862.862,0,0,0-1.219-1.219l-3.7,3.7-1.115-1.115a.862.862,0,0,0-1.219,1.219Z" transform="translate(-337.634 -230.234)"/>
                    <path id="Path_2791" data-name="Path 2791" class="cls-1" d="M377.537,389.875a.862.862,0,0,0,1.219,0l4.31-4.31a.862.862,0,0,0-1.219-1.219l-3.7,3.7-1.115-1.115a.862.862,0,0,0-1.219,1.219Z" transform="translate(-337.634 -345.305)"/>
                  </g>
                </g>
              </g>
            </svg>
            <div>
              <p class="title">Ваша думка про якість транспортного обслуговування в Чернігові дуже важлива</p>
              <p class="subtitle">Запитанять в консультаціях: 10</p>
            </div>
          </div>
          <div class="content">
            <div class="default">
              <p>Praesent sed felis fringilla, mattis quam ut, vulputate ligula. Fusce ut bibendum velit. Nullam turpis neque, blandit id elementum vitae, pulvinar sit amet lorem. Aenean ac odio eu libero eleifend tempus sit amet mattis felis. Donec rhoncus ante in lobortis fringilla. Aliquam auctor facilisis scelerisque. Curabitur eget turpis at ante maximus molestie sed mollis nunc. Aenean vitae sem vitae dolor semper venenatis ut non quam. Nullam aliquet, arcu at sodales pretium, nulla nisl egestas mi, eu mollis felis ante non diam. Etiam velit dui, finibus in nulla eu, tempus pharetra neque. Aliquam eu felis id nisl luctus imperdiet. Mauris sollicitudin vestibulum semper. Proin sit amet molestie lorem. Nam finibus magna magna, vel gravida arcu blandit vitae.</p>
              <p class="museo500">Пройшовши консультації, ви допоможете керівництву провести поліпшення <br>з урахуванням побажань мешканців міста</p>
              <a href="#" class="btn yellow-btn survey-toggle">Прийняти участь</a>
            </div>
            <div class="question question-1">
              <p class="question-number">Запитання 1 з 10</p>
              <div class="progress-bar">
                <div class="bar" style="width: 10%"></div>
              </div>
              <p class="question-text">В цьому році ми реалізували комплекс заходів, направлені на поліпшення транспортної системи міста Чернігова. Чи змінилася ситуація в кращу сторону?</p>
              <div class="mark">
                <p>0 - все очень плохо, 10 - все отлично</p>
                <form action="#">
                  <div class="line">
                    <label>
                      <input type="radio" name="mark">
                      <div><span></span></div>
                      <p>0</p>
                    </label>
                    <div class="separator"></div>
                    <label>
                      <input type="radio" name="mark">
                      <div><span></span></div>
                      <p>1</p>
                    </label>
                    <div class="separator"></div>
                    <label>
                      <input type="radio" name="mark">
                      <div><span></span></div>
                      <p>2</p>
                    </label>
                    <div class="separator"></div>
                    <label>
                      <input type="radio" name="mark">
                      <div><span></span></div>
                      <p>3</p>
                    </label>
                    <div class="separator"></div>
                    <label>
                      <input type="radio" name="mark">
                      <div><span></span></div>
                      <p>4</p>
                    </label>
                    <div class="separator"></div>
                    <label>
                      <input type="radio" name="mark">
                      <div><span></span></div>
                      <p>5</p>
                    </label>
                    <div class="separator"></div>
                    <label>
                      <input type="radio" name="mark">
                      <div><span></span></div>
                      <p>6</p>
                    </label>
                    <div class="separator"></div>
                    <label>
                      <input type="radio" name="mark">
                      <div><span></span></div>
                      <p>7</p>
                    </label>
                    <div class="separator"></div>
                    <label>
                      <input type="radio" name="mark">
                      <div><span></span></div>
                      <p>8</p>
                    </label>
                    <div class="separator"></div>
                    <label>
                      <input type="radio" name="mark">
                      <div><span></span></div>
                      <p>9</p>
                    </label>
                    <div class="separator"></div>
                    <label>
                      <input type="radio" name="mark">
                      <div><span></span></div>
                      <p>10</p>
                    </label>
                  </div>
                  <button type="submit">НАСТУПНЕ ПИТАННЯ</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="all-initiatives">
      <div class="initiatives-container">
        <div class="filter">
          <p>Всі консультації</p>
          <div class="radio-buttons">
            <label>
              <input type="radio" name="initiative-filter" checked><span class="arrow"></span><img src="img/icons/calendar.png" alt="icon">
            </label>
            <label>
              <input type="radio" name="initiative-filter"><span class="arrow"></span><img src="img/icons/tip-pen.png" alt="icon">
            </label>
          </div>
        </div>
      </div>
      <div class="container">
        <div class="messages latest-item">
          <div class="initiatives-list">
            <?php
              include 'php-components/advice-item.php';
            ?>
            <?php
              include 'php-components/advice-item.php';
            ?>
            <?php
              include 'php-components/advice-item.php';
            ?>
            <?php
              include 'php-components/advice-item.php';
            ?>
            <?php
              include 'php-components/advice-item.php';
            ?>
          </div>
          <div class="initiatives-list">
            <?php
              include 'php-components/advice-item.php';
            ?>
            <?php
              include 'php-components/advice-item.php';
            ?>
            <?php
              include 'php-components/advice-item.php';
            ?>
            <?php
              include 'php-components/advice-item.php';
            ?>
            <?php
              include 'php-components/advice-item.php';
            ?>
          </div>
        </div>
      </div>
    </div>
  </main>
    
  <?php
    include 'php-components/footer.php';
  ?>

  <!-- Scripts -->
    <!-- Libs -->
      <script defer src="js/libs.min.js"></script>
    <!-- Common -->
      <script defer src="js/common.min.js"></script>
    <!-- Custom -->
      <script defer src="js/advices.min.js"></script>
</body>
</html>