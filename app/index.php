<!DOCTYPE html>
<html lang="ua">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="HandheldFriendly" content="true">

  <meta name="description" content="This is description">
  <meta name="keywords" content="keywords">
  <title>Активний сусід</title>
  <link rel="shortcut icon" href="img/favicon.jpg" type="image/x-icon">
  <link rel="icon" href="img/favicon.jpg" type="image/x-icon">

  <style>
    #page-preloader {
      position: fixed;
      left: 0;
      top: 0;
      right: 0;
      bottom: 0;
      background: #63b6ec;
      z-index: 100500;
    }
    #page-preloader .spinner {
      width: 52px;
      height: 52px;
      position: absolute;
      left: 50%;
      top: 50%;
      margin: -26px 0 0 -26px;
      background: url('img/favicon.png') no-repeat 50% 50%;
      -webkit-animation-duration: 1s;
      animation-duration: 1s;
      -webkit-animation-fill-mode: both;
      animation-fill-mode: both;
    }
    @-webkit-keyframes preloader-anim {
      0% {
        opacity: 1;
        -webkit-transform: translateY(100px) translateX(-100px);
        transform: translateY(100px) translateX(-100px);
      }

      100% {
        opacity: 1;
        -webkit-transform: translateY(-100px) translateX(100px);
        transform: translateY(-100px) translateX(100px);
      }
    }
    @keyframes preloader-anim {
      0% {
        opacity: 1;
        -webkit-transform: translateY(100px) translateX(-100px);
        -ms-transform: translateY(100px) translateX(-100px);
        transform: translateY(100px) translateX(-100px);
      }

      100% {
        opacity: 1;
        -webkit-transform: translateY(-100px) translateX(100px);
        -ms-transform: translateY(-100px) translateX(100px);
        transform: translateY(-100px) translateX(100px);
      }
    }
    .preloader-anim {
      -webkit-animation-name: preloader-anim;
      animation-name: preloader-anim;
    }
  </style>

  <!-- Styles -->
    <!-- Libs -->
      <link rel="stylesheet" type="text/css" href="css/libs.css">
    <!-- Common -->
      <link rel="stylesheet" type="text/css" href="css/common.css">
    <!-- Custom -->
      <link rel="stylesheet" type="text/css" href="css/index.css">

  <!--[if lt IE 9]>v
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyC6HjX8Vp_hwJmQisJGsQfaC8jXIokZUxA&extension=.js"></script>
  <script>
    function addYourLocationButton(map, marker) 
    {
        var controlDiv = document.createElement('div');
        controlDiv.className = "my_location";
        
        var firstChild = document.createElement('button');
        firstChild.style.backgroundColor = '#3498DB';
        firstChild.style.border = 'none';
        firstChild.style.outline = 'none';
        firstChild.style.width = '40px';
        firstChild.style.height = '40px';
        firstChild.style.borderRadius = '2px';
        firstChild.style.boxShadow = '0 1px 4px rgba(0,0,0,0.3)';
        firstChild.style.cursor = 'pointer';
        firstChild.style.marginRight = '10px';
        firstChild.style.padding = '0px';
        firstChild.title = 'Your Location';
        controlDiv.appendChild(firstChild);
        
        var secondChild = document.createElement('div');
        secondChild.style.margin = '0';
        secondChild.style.width = '40px';
        secondChild.style.height = '40px';
        secondChild.style.backgroundImage = 'url(img/icons/cursor.png)';
        secondChild.style.backgroundPosition = 'center';
        secondChild.style.backgroundRepeat = 'no-repeat';
        secondChild.id = 'you_location_img';
        firstChild.appendChild(secondChild);
        
        google.maps.event.addListener(map, 'dragend', function() {
            $('#you_location_img').css('background-position', 'center');
        });

        firstChild.addEventListener('click', function() {
            var imgX = '0';
            var animationInterval = setInterval(function(){
                if(imgX == '-18') imgX = '0';
                else imgX = '-18';
                $('#you_location_img').css('background-position', 'center');
            }, 500);
            if(navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position) {
                    var latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                    marker.setPosition(latlng);
                    map.setCenter(latlng);
                    clearInterval(animationInterval);
                    $('#you_location_img').css('background-position', 'center');
                });
            }
            else{
                clearInterval(animationInterval);
                $('#you_location_img').css('background-position', 'center');
            }
        });
        
        controlDiv.index = 1;
        map.controls[google.maps.ControlPosition.RIGHT_CENTER].push(controlDiv);
    }
  </script>

</head>
<body>
  
  <!-- START Preloader -->
    <div id="page-preloader"><span class="spinner preloader-anim"></span></div>
  <!-- Finish Preloader -->

  <?php
    include 'php-components/map.php';
  ?>

  <?php
    include 'php-components/header.php';
  ?>
  
  <main>
    <div class="cover">
      <div class="cover-insert">
        <img src="img/index/ukraine.png" alt="img">
        <div class="right">
          <div class="info">
            <div class="info-item">
              <p class="number">29 985</p>
              <p>проблем вирішено</p>
            </div>
            <div class="info-item">
              <p class="number">29 985</p>
              <p>проблем вирішено</p>
            </div>
            <div class="info-item">
              <p class="number">29 985</p>
              <p>проблем вирішено</p>
            </div>
          </div>
          <div class="city">
            <p>Оберіть своє місто</p>
            <input type="text" class="modal-choose-the-city-init">
            <a href="#" class="go-to">Перейти</a>
            <a href="#" class="add-problem modal-add-problem-init"><img src="img/icons/pen.png" alt="img">Додати проблему</a>
          </div>
          <form action="#">
            <input type="text" placeholder="Ведіть номер проблеми або назву...">
            <button type="submit"><img src="img/icons/search.png" alt="img"></button>
          </form>
        </div>
      </div>
    </div>

    <div class="latest">
      <div class="container">
        <div class="messages latest-item">
          <div class="latest-title">
            <span></span>
            <p>Останні повідомлення</p>
            <span></span>
          </div>
          <div class="problems-list">
            <?php
              include 'php-components/problem-item.php';
            ?>
            <?php
              include 'php-components/problem-item.php';
            ?>
            <?php
              include 'php-components/problem-item.php';
            ?>
            <?php
              include 'php-components/problem-item.php';
            ?>
            <?php
              include 'php-components/problem-item.php';
            ?>
          </div>
        </div>
        <div class="ivents latest-item">
          <div class="latest-title">
            <span></span>
            <p>Останні заходи</p>
            <span></span>
          </div>
          <div class="problems-list">
            <?php
              include 'php-components/problem-item.php';
            ?>
            <?php
              include 'php-components/problem-item.php';
            ?>
            <?php
              include 'php-components/problem-item.php';
            ?>
            <?php
              include 'php-components/problem-item.php';
            ?>
            <?php
              include 'php-components/problem-item.php';
            ?>
          </div>
        </div>
      </div>
    </div>
  </main>
    
  <?php
    include 'php-components/footer.php';
  ?>

  <!-- Modals -->
    <div class="ui modal modal-dropdown modal-choose-the-city">
      <div class="content">
        <p class="modal-title">
          Оберіть вашу громаду
        </p>
        <div class="modal-content">
          <form action="#">
            <input type="text" class="search" placeholder="Введіть назву міста, селища, області чи району">
            <div class="filter">
              <label>
                <input type="radio" name="filter"> <span>Найбільш активні громади</span>
              </label>
              <label>
                <input type="radio" name="filter"> <span>Згруповані за областями</span>
              </label>
            </div>
            <div class="cols">
              <label>
                <input type="radio" name="city"> <span>Чернігів</span>
              </label>
              <label>
                <input type="radio" name="city"> <span>Чернігів</span>
              </label>
              <label>
                <input type="radio" name="city"> <span>Чернігів</span>
              </label>
              <label>
                <input type="radio" name="city"> <span>Чернігів</span>
              </label>
              <label>
                <input type="radio" name="city"> <span>Чернігів</span>
              </label>
              <label>
                <input type="radio" name="city"> <span>Чернігів</span>
              </label>
              <label>
                <input type="radio" name="city"> <span>Чернігів</span>
              </label>
              <label>
                <input type="radio" name="city"> <span>Чернігів</span>
              </label>
              <label>
                <input type="radio" name="city"> <span>Чернігів</span>
              </label>
              <label>
                <input type="radio" name="city"> <span>Чернігів</span>
              </label>
              <label>
                <input type="radio" name="city"> <span>Чернігів</span>
              </label>
              <label>
                <input type="radio" name="city"> <span>Чернігів</span>
              </label>
              <label>
                <input type="radio" name="city"> <span>Чернігів</span>
              </label>
              <label>
                <input type="radio" name="city"> <span>Чернігів</span>
              </label>
              <label>
                <input type="radio" name="city"> <span>Чернігів</span>
              </label>
              <label>
                <input type="radio" name="city"> <span>Чернігів</span>
              </label>
              <label>
                <input type="radio" name="city"> <span>Чернігів</span>
              </label>
              <label>
                <input type="radio" name="city"> <span>Чернігів</span>
              </label>
              <label>
                <input type="radio" name="city"> <span>Чернігів</span>
              </label>
              <label>
                <input type="radio" name="city"> <span>Чернігів</span>
              </label>
              <label>
                <input type="radio" name="city"> <span>Чернігів</span>
              </label>
              <label>
                <input type="radio" name="city"> <span>Чернігів</span>
              </label>
              <label>
                <input type="radio" name="city"> <span>Чернігів</span>
              </label>
              <label>
                <input type="radio" name="city"> <span>Чернігів</span>
              </label>
              <label>
                <input type="radio" name="city"> <span>Чернігів</span>
              </label>
              <label>
                <input type="radio" name="city"> <span>Чернігів</span>
              </label>
              <label>
                <input type="radio" name="city"> <span>Чернігів</span>
              </label>
              <label>
                <input type="radio" name="city"> <span>Чернігів</span>
              </label>
              <label>
                <input type="radio" name="city"> <span>Чернігів</span>
              </label>
              <label>
                <input type="radio" name="city"> <span>Чернігів</span>
              </label>
              <label>
                <input type="radio" name="city"> <span>Чернігів</span>
              </label>
              <label>
                <input type="radio" name="city"> <span>Чернігів</span>
              </label>
              <label>
                <input type="radio" name="city"> <span>Чернігів</span>
              </label>
              <label>
                <input type="radio" name="city"> <span>Чернігів</span>
              </label>
              <label>
                <input type="radio" name="city"> <span>Чернігів</span>
              </label>
              <label>
                <input type="radio" name="city"> <span>Чернігів</span>
              </label>
              <label>
                <input type="radio" name="city"> <span>Чернігів</span>
              </label>
              <label>
                <input type="radio" name="city"> <span>Чернігів</span>
              </label>
              <label>
                <input type="radio" name="city"> <span>Чернігів</span>
              </label>
              <label>
                <input type="radio" name="city"> <span>Чернігів</span>
              </label>
            </div>
            <a href="#">Не знайшли свою громаду?</a>
          </form>
        </div>
        <div class="actions">
          <a href="#" class="btn btn-outline cancel">
            <img src="img/icons/close-gray.png" alt="icon">
          </a>
          <a href="#" class="clear">Скасувати</a>
        </div>
      </div>
    </div>
  
  <!-- Scripts -->
    <!-- Libs -->
      <script defer src="js/libs.min.js"></script>
    <!-- Common -->
      <script defer src="js/common.min.js"></script>
</body>
</html>