// Map
	google.maps.event.addDomListener(window, 'load', init);
    var map, markersArray = [];
    function init() {
        var mapOptions = {
            center: new google.maps.LatLng(51.494853, 31.297662),
            zoom: 14,
            gestureHandling: 'auto',
            fullscreenControl: false,
            zoomControl: true,
            disableDoubleClickZoom: true,
            mapTypeControl: false,
            scaleControl: false,
            scrollwheel: true,
            streetViewControl: false,
            draggable : true,
            clickableIcons: true,
            zoomControlOptions: {
                position: google.maps.ControlPosition.RIGHT_CENTER
            },
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            styles: []
        };

            var mapElement = document.getElementById('map-problem');

            var map = new google.maps.Map(mapElement, mapOptions);
            
            var locations = [
                    {"title":"","address":"Text","desc":"","tel":"","int_tel":"","email":"","web":"","web_formatted":"","open":"","time":"","lat":51.494853,"lng": 31.297662,"vicinity":"Text","open_hours":"","marker":"img/icons/marker-orange.png","iw":{"address":true,"desc":true,"email":true,"enable":true,"int_tel":true,"open":true,"open_hours":true,"photo":true,"tel":true,"title":true,"web":true}}
                ];
                for (i = 0; i < locations.length; i++) {
                    marker = new google.maps.Marker({
                        icon: locations[i].marker,
                        position: new google.maps.LatLng(locations[i].lat, locations[i].lng),
                        map: map,
                        title: locations[i].title,
                        address: locations[i].address,
                        desc: locations[i].desc,
                        tel: locations[i].tel,
                        int_tel: locations[i].int_tel,
                        vicinity: locations[i].vicinity,
                        open: locations[i].open,
                        open_hours: locations[i].open_hours,
                        photo: locations[i].photo,
                        time: locations[i].time,
                        email: locations[i].email,
                        web: locations[i].web,
                        iw: locations[i].iw
                    });
                    markersArray.push(marker);
                }

            var imageMyPos = 'img/icons/my-location.svg';
            var myMarker = new google.maps.Marker({
                map: map,
                animation: google.maps.Animation.DROP,
                position: {lat: 511.494992, lng: 311.301912},
                icon: imageMyPos
            });
            addYourLocationButton(map, myMarker);
        }