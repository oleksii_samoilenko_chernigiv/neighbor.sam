$(document).ready(function() {

	$(".map-toggler").click(function(e) {
		e.preventDefault();
		$('.map').toggleClass('open');
  });

	$(".map .type").click(function(e) {
		e.preventDefault();
		$( this ).toggleClass('choosen');
  });

  //Tabs init
    $( function() {
      $( "#tabs" ).tabs();
    });

  // Login Modal Init
    $('.modal-login-init').click(function(e){
      e.preventDefault();
        $('.modal-login')
          .modal({
            // transition: 'vertical flip',
            autofocus: false,
            centered: false,
            inverted: true
          })
          // .modal({ observeChanges: false })
          .modal('show')
        ;
    });

  // Registration Modal Init
    $('.modal-registration-init').click(function(e){
      e.preventDefault();
        $('.modal-registration')
          .modal({
            // transition: 'vertical flip',
            autofocus: false,
            centered: false,
            inverted: true
          })
          // .modal({ observeChanges: false })
          .modal('show')
        ;
    });

  // Thanks Registration Modal Init
    $('.modal-mail-check-init').click(function(e){
      e.preventDefault();
        $('.modal-mail-check')
          .modal({
            // transition: 'vertical flip',
            autofocus: false,
            centered: false,
            inverted: true
          })
          // .modal({ observeChanges: false })
          .modal('show')
        ;
    });

  // Thanks Registration Modal Init
    $('.modal-password-init').click(function(e){
      e.preventDefault();
        $('.modal-password')
          .modal({
            // transition: 'vertical flip',
            autofocus: false,
            centered: false,
            inverted: true
          })
          // .modal({ observeChanges: false })
          .modal('show')
        ;
    });

  // Thanks Registration Modal Init
    $('.modal-add-problem-init').click(function(e){
      e.preventDefault();
        $('.modal-add-problem')
          .modal({
            // transition: 'vertical flip',
            autofocus: false,
            centered: false,
            inverted: true
          })
          // .modal({ observeChanges: false })
          .modal('show')
        ;
    });

  // Thanks Choose The City Modal Init
    $('.modal-choose-the-city-init').click(function(e){
      e.preventDefault();
        $('.modal-choose-the-city')
          .modal({
            // transition: 'vertical flip',
            autofocus: false,
            centered: false
          })
          // .modal({ observeChanges: false })
          .modal('show')
        ;
    });

  // Thanks Choose the type Modal Init
    $('.modal-choose-the-type-init').click(function(e){
      e.preventDefault();
        $('.modal-choose-the-type')
          .modal({
            // transition: 'vertical flip',
            autofocus: false,
            centered: false
          })
          // .modal({ observeChanges: false })
          .modal('show')
        ;
    });

  // Thanks Choose the defedant Modal Init
    $('.modal-choose-the-defendant-init').click(function(e){
      e.preventDefault();
        $('.modal-choose-the-defendant')
          .modal({
            // transition: 'vertical flip',
            autofocus: false,
            centered: false
          })
          // .modal({ observeChanges: false })
          .modal('show')
        ;
    });

  // Treatment Modal Init
    $('.modal-treatment-init').click(function(e){
      e.preventDefault();
        $('.modal-treatment')
          .modal({
            // transition: 'vertical flip',
            autofocus: false,
            centered: false,
            inverted: true
          })
          // .modal({ observeChanges: false })
          .modal('show')
        ;
    });

  // Treatment Modal Init
    $('.modal-treatment-thanks-init').click(function(e){
      e.preventDefault();
        $('.modal-treatment-thanks')
          .modal({
            // transition: 'vertical flip',
            autofocus: false,
            centered: false,
            inverted: true
          })
          // .modal({ observeChanges: false })
          .modal('show')
        ;
    });
});

// START Hide preloader after load page
  $(window).on('load', function () {
      var $preloader = $('#page-preloader'),
          $spinner   = $preloader.find('.spinner');
      $spinner.fadeOut();
      $preloader.delay(350).fadeOut('slow');
  });
// FINISH Hide preloader after load page